import { utils } from "near-api-js";
import { getMasterAccount } from "./blockchain";

const nearApi = require("near-api-js");
const nearSeedPhrase = require("near-seed-phrase");

// Generates random public private key pair using fromRandom function defined within near utils
export const generateKeyPair = async () => {
  const keypair = nearApi.utils.KeyPair.fromRandom("ed25519");

  return {
    public_key: keypair.publicKey.toString(),
    private_key: keypair.secretKey,
  };
};

// generates keypair using keys parsed from passphrase
export const createKeyPair = async (name: string) => {
  // generates seed phrase as well as public private key for the account
  const passPhraseData = nearSeedPhrase.generateSeedPhrase();

  const account = {
    account_id: name,
    public_key: passPhraseData.publicKey,
    private_key: passPhraseData.secretKey,
    passPhrase: passPhraseData.seedPhrase,
  };

  return account;
};

/**
 * @return {boolean}
 */
//creates account using master account details
export const createAccount = async (new_account: {
  account_id: string;
  public_key: string;
}) => {
  // gets the details of master account id connected to near testnet network
  const account = await getMasterAccount();

  // creates a new account with the amount taken from master account
  // new account is created with the public key
  //default amount with which the new account is to be created in yocto near format
  // 1 yocto = 10^-24 Near
  const amountInYocto = utils.format.parseNearAmount("1");

  const res = await account.createAccount(
    new_account.account_id,
    new_account.public_key,

    amountInYocto
  );

  try {
    if (res["status"].hasOwnProperty("SuccessValue")) {
      return true;
    }
  } catch (e) {
    console.log(e);
  }

  return false;
};
// function that generates keys from a given seed phrase
export const getKeysFromSeedPhrase = async (seedPhrase: string) => {
  return nearSeedPhrase.parseSeedPhrase(seedPhrase);
};
