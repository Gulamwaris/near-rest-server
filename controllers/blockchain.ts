const nearApi = require("near-api-js");
const fs = require("fs");
import axios from "axios";
import { CONFIG_PATH, notify, reject } from "../helpers/api";

let settings: any;
// Check if near-api-server.config.json file exists
if (fs.existsSync(CONFIG_PATH)) {
  // if exists , take settings from the file
  settings = JSON.parse(fs.readFileSync(CONFIG_PATH, "utf8"));
} else {
  const data = {
    server_host: "localhost",
    server_port: 3000,
    rpc_node: "https://rpc.testnet.near.org",
    allow_rpc_update: false,
  };
  // if file doesnt exists m create and write to file
  fs.writeFileSync(
    CONFIG_PATH,
    JSON.stringify(data),
    { flag: "wx" },
    function (err: any) {
      if (err) {
        console.log(err);
      }
    }
  );
  settings = JSON.parse(fs.readFileSync(CONFIG_PATH, "utf8"));
}

export const initContract = async (
  master_account_id: string,
  master_key: string,
  nft_contract: string,
  server_host: string,
  server_port: string,
  rpc_node: string
) => {
  try {
    const new_settings = settings;
    if (master_account_id) new_settings.master_account_id = master_account_id;
    if (master_key) new_settings.master_key = master_key;
    if (nft_contract) new_settings.nft_contract = nft_contract;
    if (server_host) new_settings.server_host = server_host;
    if (server_port) new_settings.server_port = server_port;
    if (rpc_node) new_settings.rpc_node = rpc_node;

    await fs.promises.writeFile(
      CONFIG_PATH,
      JSON.stringify({
        ...new_settings,
      })
    );

    return notify("Settings updated.");
  } catch (e) {
    return reject(e);
  }
};

export const getMasterAccount = async () => {
  try {
    // creating a key pair with the key stored for master's account id
    const keyPair = nearApi.utils.KeyPair.fromString(settings.master_key);
    // creating a placeholder for storing the keys
    const keyStore = new nearApi.keyStores.InMemoryKeyStore();
    // setting up the keystore
    keyStore.setKey("testnet", settings.master_account_id, keyPair);

    // configurations for connecting to the near network
    const near = await nearApi.connect({
      networkId: "testnet",
      keyStore,
      masterAccount: settings.master_account_id,
      nodeUrl: settings.rpc_node,
    });
    // returns account connected with the near network
    return await near.account(settings.master_account_id);
  } catch (e) {
    return reject(e);
  }
};

export const getAccountByKey = async (
  account_id: string,
  private_key: string
) => {
  try {
    private_key = private_key.replace('"', "");

    const keyPair = nearApi.utils.KeyPair.fromString(private_key);
    const keyStore = new nearApi.keyStores.InMemoryKeyStore();
    keyStore.setKey("testnet", account_id, keyPair);

    const near = await nearApi.connect({
      networkId: "testnet",
      keyStore,
      masterAccount: account_id,
      nodeUrl: settings.rpc_node,
    });

    return await near.account(account_id);
  } catch (e) {
    return reject(e);
  }
};
