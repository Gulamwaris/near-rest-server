import { Router, Request, Response, NextFunction } from "express";
import { initContract } from "./controllers/blockchain";
import { CONFIG_PATH, ok } from "./helpers/api";
const nearApi = require("near-api-js");
import fs from "fs";
import {
  createAccount,
  createKeyPair,
  getKeysFromSeedPhrase,
} from "./controllers/users";
import axios from "axios";
const { decode } = require("bs58");

const router = Router();

/**
 * @description Initializes the repo with master account
 * @requires
 * master_account_id : Account id with which this repo will be initialized
 * seed_phrase : Seed phrase of master account
 * private_key :private key of master account
 * nft_contract : default set to master_account_id
 * server_host : default set to localhost
 * server_port :default set to 3000
 * rpc_node : https://rpc.testnet.near.org
 */
router.post(
  "/init",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const settings = await JSON.parse(fs.readFileSync(CONFIG_PATH, "utf8"));
      if (settings.init_disabled) {
        throw new Error("Method now allowed");
      }
      // get details from body
      let {
        master_account_id,
        seed_phrase,
        private_key,
        nft_contract,
        server_host,
        server_port,
        rpc_node,
      } = req.body;

      // gets secret key from the passphrase provided
      if (seed_phrase)
        private_key = (await getKeysFromSeedPhrase(seed_phrase)).secretKey;

      // Initialized and stores the details in the file
      const result: any = await initContract(
        master_account_id,
        private_key,
        nft_contract,
        server_host,
        server_port,
        rpc_node
      );

      ok(req, res, result);
    } catch (err) {
      next(err);
    }
  }
);

/**
 * @description This endpoint is used to create an account with near
 * @requires
 * name : Name of account to be created with near platform.
 *
 */
router.post(
  "/create_user",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      // get details of the master account id
      const settings = await JSON.parse(fs.readFileSync(CONFIG_PATH, "utf8"));

      // the name of the sub-account id to be created
      const name = (
        req.body.name +
        "." +
        settings.master_account_id
      ).toLowerCase();

      // generated public/private key pair as well as passphrase to be associated with the account id
      const account = await createKeyPair(name);

      const data = {
        account_id: account.account_id,
        public_key: account.public_key,
        private_key: account.private_key,
      };
      // function call to create an account with the master account id
      const isAccountCreated = await createAccount(data);

      if (isAccountCreated)
        return ok(req, res, {
          publicKey: `${data.public_key}`,
          accountName: `${name}`,
          privateKey: `${data.private_key}`,
          passPhrase: `${account.passPhrase}`,
          text: `Account ${name} created. `,
        });
      ok(req, res, { text: "Error creating account" });
    } catch (err) {
      next(err);
    }
  }
);

/**
 * @description Gets user keys from passphrase
 * @requires
 * passphrase - twelve word passphrase given at time of account creation.
 *
 */
router.post(
  "/user_details",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const account = await getKeysFromSeedPhrase(
        req.body.seed_phrase.toLowerCase()
      );
      let accountId;
      let doesAccountExist = true;
      const URL =
        "https://helper.testnet.near.org/publicKey/" +
        account.publicKey +
        "/accounts";
      try {
        const result: any = await axios.get(URL);
        if (!result.data[0]) {
          doesAccountExist = false;
        } else {
          accountId = result.data[0];
        }
      } catch (err) {
        console.log("err: ", err);
        doesAccountExist = false;
      }

      if (!doesAccountExist) {
        return ok(req, res, {
          text: "Error fetching account details",
          success: false,
        });
      }
      ok(req, res, {
        publicKey: `${account.publicKey}`,
        accountName: accountId,
        privateKey: `${account.secretKey}`,
        success: true,
      });
    } catch (err) {
      next(err);
    }
  }
);

router.post(
  "/parse_seed_phrase",
  async (req: Request, res: Response, next: NextFunction) => {
    try {
      const account = await getKeysFromSeedPhrase(
        req.body.seed_phrase.toLowerCase()
      );

      const hexPublicKey = decode(
        account.publicKey.replace("ed25519:", "")
      ).toString("hex");
      ok(req, res, account);
    } catch (err) {
      next(err);
    }
  }
);

export default router;
