import express, { NextFunction, Request, Response } from "express";

// Creating a router instance using express
const router = express.Router();

/**
 * @route /api/health-check/
 * @description
 *  - sends status 200 and a message 'Success' in response
 *
 */
router.get("/", async (req: Request, res: Response, next: NextFunction) => {
  try {
    res.status(200).send({
      message: "Success",
      status: 200,
    });
  } catch (err) {
    next(err);
  }
});

export default router;
