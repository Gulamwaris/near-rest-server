import { NextFunction, Response, Request } from "express";

export default (req: Request, res: Response, next: NextFunction) => {
  // Get the encrypted data field from the request body
  const { encryptedData, publicKey } = req.body;
  // If encryptedData field does not exist, return
  if (!encryptedData) {
    return next();
  }

  try {
    // If encryptedData is present in the request body, use the decrypt function in the app locals to decrypt it
    const decryptedData = req.app.locals.decryptMessage(encryptedData);

    req.body = { ...JSON.parse(decryptedData), publicKey };
    next();
  } catch (err) {
    console.log("err: ", err);
    res.status(406).json({
      status: 406,
      message: "Public key possibly incorrect. Re-initiate session.",
    });
  }
};
