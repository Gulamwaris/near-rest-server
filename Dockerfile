# Install dependencies only when needed
FROM node:12-buster-slim AS builder

WORKDIR /app

COPY . .

RUN rm -rf .git

USER root

RUN yarn install

RUN ./node_modules/typescript/bin/tsc

#RUN yarn build --production --ignore-scripts --prefer-offline
RUN ls

# Production image, copy all the files and run next
FROM node:12-buster-slim AS runner

WORKDIR /app

COPY --from=builder /app/ /app/

# Setting permissions for all folders
RUN chown -R node:node /app

# Using non-root user to run the application
USER node

RUN ls -alrt

EXPOSE 3000
# Next.js collects completely anonymous telemetry data about general usage.
# Learn more here: https://nextjs.org/telemetry
# Uncomment the following line in case you want to disable telemetry.
# ENV NEXT_TELEMETRY_DISABLED 1

CMD ["node", "index.js"]