import express from "express";
import helmet from "helmet";
import morgan from "morgan";
import { json, urlencoded } from "body-parser";
import cors from "cors";
import routes from "./routes";
import CryptoGraphy from "./helpers/cryptography";
import decrypt from "./middlewares/decrypt";
import healthCheck from "./health-check";

const app = express();

app.set("trust-proxy", 1);

// Block all unwanted headers using helmet
app.use(helmet());

// Disable x-powered-by header separately
app.disable("x-powered-by");

//Setup server
app.use(cors());
app.use(json());
app.use(
  urlencoded({
    extended: false,
  })
);
app.disable("etag"); //Disables caching

app.use(
  morgan("common", {
    skip: (req, res) => req.baseUrl?.includes("health-check"),
  })
);

app.use("/", decrypt, routes);
app.use("/health-check", healthCheck);

//Check if port exists in the environment else use 3000
const port = process.env.PORT || 3000;

//If the environment is test, do not start the express server
if (process.env.NODE_ENV !== "test") {
  app
    .listen(parseInt(port.toString()), "0.0.0.0", () => {
      // Initialize encryption/decryption module
      const cryptoGraphy = new CryptoGraphy();

      // Storing getPublicKey and decrypt function in the app locals
      app.locals.getPublicKey = cryptoGraphy.getPublicKey;
      app.locals.decryptMessage = cryptoGraphy.decryptMessage;
      app.locals.encryptMessage = cryptoGraphy.encryptMessage;
      //Listen the express server on the given port and log a message to the logs
      console.info(`Server is listening on port ${port}`);
    })
    .on("error", (err: any) => {
      //In case of an error, log the error to the logs
      console.error(JSON.stringify(err));
    });
}
