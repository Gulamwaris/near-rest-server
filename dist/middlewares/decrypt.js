"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.default = (req, res, next) => {
    // Get the encrypted data field from the request body
    const { encryptedData, publicKey } = req.body;
    // If encryptedData field does not exist, return
    if (!encryptedData) {
        return next();
    }
    try {
        // If encryptedData is present in the request body, use the decrypt function in the app locals to decrypt it
        const decryptedData = req.app.locals.decryptMessage(encryptedData);
        req.body = Object.assign(Object.assign({}, JSON.parse(decryptedData)), { publicKey });
        next();
    }
    catch (err) {
        console.log("err: ", err);
        res.status(406).json({
            status: 406,
            message: "Public key possibly incorrect. Re-initiate session.",
        });
    }
};
//# sourceMappingURL=decrypt.js.map