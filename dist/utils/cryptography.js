"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const node_rsa_1 = __importDefault(require("node-rsa"));
const fs_1 = __importDefault(require("fs"));
class CryptoGraphy {
    constructor() {
        // Method to decrypt a given string
        this.decryptMessage = (encryptedMessage) => {
            return this.privateKeyCrypto.decrypt(encryptedMessage, "utf8");
        };
        // Method to encrypt a given string and with the given public key
        this.encryptMessage = (message, publicKey) => {
            const customPublicKeyCrypto = new node_rsa_1.default(publicKey);
            customPublicKeyCrypto.setOptions({ encryptionScheme: "pkcs1" });
            return customPublicKeyCrypto.encrypt(message, "base64");
        };
        this.getPublicKey = () => {
            return this.publicKey;
        };
        this.publicKey = fs_1.default.readFileSync(`${process.cwd()}/rsa_2048_pub.pem`, {
            encoding: "utf8",
        });
        this.privateKey = fs_1.default.readFileSync(`${process.cwd()}/rsa_2048_priv.pem`, {
            encoding: "utf8",
        });
        this.privateKeyCrypto = new node_rsa_1.default(this.privateKey);
        this.publicKeyCrypto = new node_rsa_1.default(this.publicKey);
        this.privateKeyCrypto.setOptions({ encryptionScheme: "pkcs1" });
        this.publicKeyCrypto.setOptions({ encryptionScheme: "pkcs1" });
    }
}
exports.default = CryptoGraphy;
//# sourceMappingURL=cryptography.js.map