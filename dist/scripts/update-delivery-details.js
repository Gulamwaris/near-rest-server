"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const orders_1 = require("../repositories/orders");
const logger_1 = __importDefault(require("../utils/logger"));
exports.default = () => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    logger_1.default.info(`Starting to update delivery details of the customer who have not responded to the delivery`);
    try {
        // getting all the Delivered orders which are not confirmed by Rentee side
        const result = yield (0, orders_1.getAllDeliveredOrders)();
        const deliveredOrders = result.data;
        for (let i = 0; i < (deliveredOrders === null || deliveredOrders === void 0 ? void 0 : deliveredOrders.length); i++) {
            const hoursAfterDelivery = hoursDifference(deliveredOrders[i].deliveredOn);
            if (hoursAfterDelivery > 24) {
                try {
                    yield (0, orders_1.updateDeliveryDetails)({
                        orderId: deliveredOrders[i].orderId,
                        deliveryDetails: {},
                    });
                }
                catch (error) {
                    throw {
                        code: 1001,
                        message: "Unable to update delivery details in DB",
                    };
                }
                try {
                    yield axios_1.default.post("http://localhost:3000/transferToRenter", {
                        orderId: deliveredOrders[i].orderId,
                        renterAccountId: (_a = deliveredOrders[i]) === null || _a === void 0 ? void 0 : _a.renterAccountId,
                    });
                }
                catch (err) {
                    throw {
                        code: 4001,
                        message: "Unable to transfer amount to Renter",
                        stack: err,
                    };
                }
            }
        }
        logger_1.default.info(`Updated Delivery details of the orders`);
    }
    catch (err) {
        // log the err to the console
        logger_1.default.error(`at: "scripts/update-payment-limit/updatePaymentLimitForAll",
    ${JSON.stringify(err)}\n${err}`);
    }
});
const hoursDifference = (deliveryDate) => {
    // Getting today's date
    const todaysDate = new Date();
    let difference = (todaysDate.getTime() - deliveryDate.getTime()) / 1000;
    difference /= 60 * 60;
    difference = Math.abs(Math.round(difference));
    return difference;
};
//# sourceMappingURL=update-delivery-details.js.map