"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const orders_1 = __importDefault(require("../controllers/orders"));
const router = express_1.default.Router();
/**
 * @route for fetching orders by orderId
 *  * @description
 *  - Returns order Details for the orderId passed
 */
router.get("/:orderId", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { orderId } = req.params;
        //if any of the below parameters is missing, return an error response
        if (!orderId) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required.
          ${!orderId && "Id is required."}`,
            };
        }
        const result = yield orders_1.default.fetchOrderByOrderId(orderId);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Fetched Order Details Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for storing review details in DB
 *  * @description
 *  - Takes orderStatus, orderDuration, requestedDate, productId, renterAccountId, renteeAccountId, address,
 *  - Stores the review details in DB for a product
 * @params
 *  - @requires productId Product Id
 *  - @requires renteeAccountId Account Id of the user who has ordered
 *  - @requires orderDuration Duration of order
 *  - @requires requestedDate requested Date
 *  - @requires renterAccountId Account Id of the user on whose product the order is placed
 *  - @requires address Address where order needs to be delivered
 */
router.post("/", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Get required fields from the req body
        const { orderDuration, orderStartDate, orderEndDate, productId, renterAccountId, renteeAccountId, address, sellerName, productPrice, productName, productImage, liabilityPrice, shippingPrice, totalPrice, orderType, } = req.body;
        console.log(req.body);
        //if any of the below parameters is missing, return an error response
        if (!productId) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required.
          ${!productId && "Id is required."}`,
            };
        }
        const orderDetails = {
            orderDuration,
            orderStartDate,
            orderEndDate,
            productId,
            renterAccountId,
            renteeAccountId,
            address,
            sellerName,
            productPrice,
            productName,
            productImage,
            liabilityPrice,
            shippingPrice,
            totalPrice,
            orderType,
        };
        // Call the updateProfile controller function
        const result = yield orders_1.default.saveOrder(orderDetails);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Order Placed Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for updating token based on orderId for sending notification
 *  * @description
 *  - Takes orderId, orderStatus
 * @params
 *  - @requires orderId orderId of the user
 *  - @requires orderStatus Order status
 */
router.put("/status", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Get id and rolesfrom the req body
        const { orderId, orderStatus } = req.body;
        //if id or roles is missing, return an error response
        if (!orderId || !orderStatus) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required
            ${orderId && "Id is required."}`,
            };
        }
        const orderInfo = {
            orderId,
            orderStatus,
        };
        // Call the updateRole controller function
        const result = yield orders_1.default.updateOrderStatus(orderInfo);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            message: "Order updated Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for fetching orders by renteeAccountID
 *  * @description
 *  - Returns order Details for the accountId passed
 */
router.post("/rentee", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { accountId } = req.body;
        //if any of the below parameters is missing, return an error response
        if (!accountId) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required.
          ${!accountId && "Id is required."}`,
            };
        }
        const result = yield orders_1.default.fetchRenteeOrderByOrderId(accountId);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Fetched Order Details Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for fetching orders by renterAccountID
 *  * @description
 *  - Returns order Details for the accountId passed
 */
router.post("/renter", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { accountId } = req.body;
        //if any of the below parameters is missing, return an error response
        if (!accountId) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required.
          ${!accountId && "Id is required."}`,
            };
        }
        const result = yield orders_1.default.fetchRenterOrderByOrderId(accountId);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Fetched Order Details Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for fetching orders by renterAccountID
 *  * @description
 *  - Returns order Details for the accountId passed
 */
router.post("/status", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { accountId, orderStatus } = req.body;
        //if any of the below parameters is missing, return an error response
        if (!accountId) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required.
          ${!accountId && "Id is required."}`,
            };
        }
        const result = yield orders_1.default.fetchOrderByOrderStatus(orderStatus, accountId);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Fetched Order Details Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for fetching pednig orders
 *  * @description
 *  - Returns order Details for the accountId passed
 */
router.post("/pending", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { accountId } = req.body;
        //if any of the below parameters is missing, return an error response
        if (!accountId) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required.
          ${!accountId && "Id is required."}`,
            };
        }
        const result = yield orders_1.default.fetchPendingOrders(accountId);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Fetched Order Details Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for updating token based on orderId for sending notification
 *  * @description
 *  - Takes orderId, deliveryDetails
 * @params
 *  - @requires orderId orderId of the user
 *  - @requires deliveryDetails Details of delivery
 */
router.put("/delivery-details", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Get id and rolesfrom the req body
        const { orderId, deliveryDetails } = req.body;
        //if id or roles is missing, return an error response
        if (!orderId) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required
            ${orderId && "Id is required."}`,
            };
        }
        const orderInfo = {
            orderId,
            deliveryDetails,
        };
        // Call the updateRole controller function
        const result = yield orders_1.default.updateDeliveryDetails(orderInfo);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            message: "Delivery Details updated Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for updating token based on orderId for sending notification
 *  * @description
 *  - Takes orderId, deliveryDetails
 * @params
 *  - @requires orderId orderId of the user
 *  - @requires deliveryDetails Details of delivery
 */
router.put("/return-details", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Get id and rolesfrom the req body
        const { orderId, returnDetails } = req.body;
        //if id or roles is missing, return an error response
        if (!orderId) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required
            ${orderId && "Id is required."}`,
            };
        }
        const orderInfo = {
            orderId,
            returnDetails,
        };
        // Call the updateRole controller function
        const result = yield orders_1.default.updateOrderReturnDetails(orderInfo);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            message: "Delivery Details updated Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for updating token based on orderId for sending notification
 *  * @description
 *  - Takes orderId, deliveryDetails
 * @params
 *  - @requires orderId orderId of the user
 *  - @requires deliveryDetails Details of delivery
 */
router.put("/damage-approval", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Get id and rolesfrom the req body
        const { orderId, isApproved } = req.body;
        //if id or roles is missing, return an error response
        if (!orderId) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required
            ${orderId && "Id is required."}`,
            };
        }
        // Call the updateRole controller function
        const result = yield orders_1.default.updateDamageApprovedByRentee(isApproved, orderId);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            message: "Delivery Details updated Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
exports.default = router;
//# sourceMappingURL=orders.js.map