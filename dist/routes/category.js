"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const category_1 = __importDefault(require("../controllers/category"));
const router = express_1.default.Router();
/**
 * @route for fetching all category present in DB
 *  * @description
 *  - Returns all category from DB
 */
router.get("/all", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const result = yield category_1.default.fetchAllCategory();
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Fetched All Category Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for adding a new category in DB
 *  * @description
 *  - Takes categoryName
 *  - Stores the category in DB
 * @params
 *  - @requires categoryName First Name of the user
 */
router.post("/", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Get required fields from the req body
        const { categoryName, categoryIcon } = req.body;
        //if any of the below parameters is missing, return an error response
        if (!categoryName) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required.
          ${!categoryName && "Category Name is required."}`,
            };
        }
        const categoryDetails = {
            categoryName,
            categoryIcon,
        };
        // Call the updateProfile controller function
        const result = yield category_1.default.addCategory(categoryDetails);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "category Added Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for adding a new category in DB
 *  * @description
 *  - Takes categoryName
 *  - Stores the category in DB
 * @params
 *  - @requires categoryName First Name of the user
 */
router.post("/sub-category", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Get required fields from the req body
        const { categoryId, name, fields, icon } = req.body;
        //if any of the below parameters is missing, return an error response
        if (!categoryId) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required.
          ${!categoryId && "Category Id is required."}`,
            };
        }
        const categoryDetails = {
            categoryId,
            name,
            fields,
            icon,
        };
        // Call the updateProfile controller function
        const result = yield category_1.default.addSubCategory(categoryDetails);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Sub Category Added Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for Deleting all users present in DB
 *  * @description
 *  - Deletes all users from DB
 */
router.delete("/all", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const result = yield category_1.default.deleteAllCategory();
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Deleted All Category Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for fetching category by category Id
 *  * @description
 *  - Returns category Id
 */
router.get("/:catgeoryId", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { catgeoryId } = req.params;
        //if any of the below parameters is missing, return an error response
        if (!catgeoryId) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required.
          ${!catgeoryId && "Id is required."}`,
            };
        }
        const result = yield category_1.default.fetchCategoryById(catgeoryId);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Fetched Category Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
exports.default = router;
//# sourceMappingURL=category.js.map