"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const transactions_1 = __importDefault(require("../controllers/transactions"));
const router = express_1.default.Router();
/**
 * @route for fetching transactions by accountId
 *  * @description
 *  - Returns all transactions
 */
router.post("/user", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { accountId } = req.body;
        //if any of the below parameters is missing, return an error response
        if (!accountId) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required.
          ${!accountId && "Id is required."}`,
            };
        }
        const result = yield transactions_1.default.fetchTransactionByAccountId(accountId);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Fetched Transactions Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
router.post("/", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { transactionDesc, transactionType, orderId, amount, accountId } = req.body;
        //if any of the below parameters is missing, return an error response
        if (!accountId) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required.
          ${!accountId && "Id is required."}`,
            };
        }
        const result = yield transactions_1.default.addTransaction({
            transactionDesc,
            transactionType,
            orderId,
            amount,
            accountId,
        });
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Fetched Transactions Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
exports.default = router;
//# sourceMappingURL=transactions.js.map