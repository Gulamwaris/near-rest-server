"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const users_1 = __importDefault(require("../controllers/users"));
const router = express_1.default.Router();
/**
 * @route for fethcing all users present in DB
 *  * @description
 *  - Returns all users from DB
 */
router.get("/all", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const result = yield users_1.default.fetchAllUsers();
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Fetched All Users Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for fetching user by account Id
 *  * @description
 *  - Returns users based on athe accoutn Id passed
 */
router.get("/:accountId", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { accountId } = req.params;
        //if any of the below parameters is missing, return an error response
        if (!accountId) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required.
          ${!accountId && "Id is required."}`,
            };
        }
        const result = yield users_1.default.fetchUserByAccountId(accountId);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Fetched User Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for fetching favorites of a user
 *  * @description
 *  - Returns favorites based on athe accoutn Id passed
 */
router.post("/favorites", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { accountId } = req.body;
        //if any of the below parameters is missing, return an error response
        if (!accountId) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required.
          ${!accountId && "Id is required."}`,
            };
        }
        const result = yield users_1.default.fetchUserFavorites(accountId);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Fetched Products Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for storing user details in DB
 *  * @description
 *  - Takes accountId,firstName,lastName,email,mobileNumber,profileImage,uniqueToken,
 *  - Stores the user details in DB
 * @params
 *  - @requires accountId Account Id of the user
 *  - @requires firstName First Name of the user
 *  - @requires lastName Last Name of the user
 *  - @requires email Email of the user
 *  - @requires mobileNumber Mobile No of the user
 *  - @requires profileImage Profile Image of the user
 *  - @requires uniqueToken Token of the user
 */
router.post("/", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Get required fields from the req body
        const { accountId, firstName, lastName, email, mobileNumber, profileImage, uniqueToken, isRenter, } = req.body;
        //if any of the below parameters is missing, return an error response
        if (!accountId) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required.
          ${!accountId && "Id is required."}`,
            };
        }
        const userDetails = {
            accountId,
            firstName,
            lastName,
            email,
            mobileNumber,
            profileImage,
            uniqueToken,
            isRenter,
        };
        // Call the updateProfile controller function
        const result = yield users_1.default.storeUserDetails(userDetails);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "User Details Stored Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for updating profile details of user in DB
 *  * @description
 *  - Takes accountId,firstName,lastName,email,mobileNumber,profileImage
 *  - Stores the user details in DB
 * @params
 *  - @requires accountId Account Id of the user
 *  - @requires firstName First Name of the user
 *  - @requires lastName Last Name of the user
 *  - @requires email Email of the user
 *  - @requires mobileNumber Mobile No of the user
 *  - @requires profileImage Profile Image of the user
 */
router.put("/profile", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Get required fields from the req body
        const { accountId, firstName, lastName, email, mobileNumber, profileImage, } = req.body;
        console.log("%c 🥟 req.body: ", "font-size:20px;background-color: #B03734;color:#fff;", req.body);
        //if any of the below parameters is missing, return an error response
        if (!accountId || !firstName || !lastName || !email || !mobileNumber) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required.
        ${!accountId && "Id is required."}
        ${!firstName && " First Name is required."}
        ${!lastName && " Last Name is required."}
        ${!email && " Email is required."}
        ${!mobileNumber && " Mobile No is required."}`,
            };
        }
        const userDetails = {
            accountId,
            firstName,
            lastName,
            email,
            mobileNumber,
            profileImage,
        };
        // Call the updateProfile controller function
        const result = yield users_1.default.updateProfile(userDetails);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "User Updated Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for adding address based on accountId
 *  * @description
 *  - Takes accountId, address
 * @params
 *  - @requires accountId accountId of the user
 *  - @requires address address of the user
 */
router.post("/address", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Get id and rolesfrom the req body
        const { accountId, address } = req.body;
        //if id or roles is missing, return an error response
        if (!accountId || !address) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required
        ${accountId && "Id is required."}
        ${address && "Roles is required."}`,
            };
        }
        const userDetails = {
            accountId,
            address,
        };
        // Call the updateRole controller function
        const result = yield users_1.default.addNewAddress(userDetails);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            message: "User Address Updated Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for updating address based on accountId
 *  * @description
 *  - Takes accountId, address
 * @params
 *  - @requires accountId accountId of the user
 *  - @requires address address of the user
 */
router.put("/address", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Get id and rolesfrom the req body
        const { accountId, address } = req.body;
        //if id or roles is missing, return an error response
        if (!accountId || !address) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required
        ${accountId && "Id is required."}
        ${address && "Roles is required."}`,
            };
        }
        const userDetails = {
            accountId,
            address,
        };
        // Call the updateRole controller function
        const result = yield users_1.default.updateAddress(userDetails);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            message: "User Address Updated Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for updating token based on accountId for sending notification
 *  * @description
 *  - Takes accountId, token
 * @params
 *  - @requires accountId accountId of the user
 *  - @requires token FCM Token of the user
 */
router.put("/token", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Get id and rolesfrom the req body
        const { accountId, token } = req.body;
        //if id or roles is missing, return an error response
        if (!accountId || !token) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required
          ${accountId && "Id is required."}
          ${token && "Roles is required."}`,
            };
        }
        const userDetails = {
            accountId,
            token,
        };
        // Call the updateRole controller function
        const result = yield users_1.default.updateToken(userDetails);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            message: "User Token Updated Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for Deleting all users present in DB
 *  * @description
 *  - Deletes all users from DB
 */
router.delete("/all", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const result = yield users_1.default.deleteAllUser();
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Deleted All Users Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for managing favorite products of a users
 *  * @description
 *  - Takes accountId,productId
 *  - Updates the favorite products list of user
 * @params
 *  - @requires accountId Account Id of the user
 *  - @requires productId Product Id of the product
 */
router.post("/manage-favorites", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { accountId, productId } = req.body;
        const result = yield users_1.default.manageFavorites({
            accountId,
            productId,
        });
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        res.status(200).json({
            status: 200,
            message: "Added/Removed from Favorites successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
exports.default = router;
//# sourceMappingURL=users.js.map