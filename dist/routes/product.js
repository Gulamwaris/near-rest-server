"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const product_1 = __importDefault(require("../controllers/product"));
const logger_1 = __importDefault(require("../utils/logger"));
const router = express_1.default.Router();
router.post("/add", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { categoryId, subCategoryId, productName, price, liabilityPrice, images, sellerName, sellerAccountId, productAvailability, productDetails, isPublished, rentalDuration, bufferPeriod, isItemForRent, } = req.body;
        const data = {
            categoryId,
            subCategoryId,
            productName,
            price,
            liabilityPrice,
            images,
            sellerName,
            createdOn: new Date(),
            modifiedOn: new Date(),
            sellerAccountId,
            productAvailability,
            productDetails,
            isPublished,
            rentalDuration,
            bufferPeriod,
            isItemForRent,
        };
        if (!categoryId ||
            !productName ||
            !price ||
            !liabilityPrice ||
            !images ||
            !sellerName ||
            !sellerAccountId ||
            !productAvailability ||
            !productDetails ||
            !isPublished ||
            isItemForRent) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required`,
            };
        }
        logger_1.default.info(`Creating new Product request with catgeory: ${categoryId}`);
        // Call the createProduct controller function to store product in DB
        const result = yield product_1.default.addProduct(data);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        logger_1.default.info(`Model added successfully`);
        // return success response
        res.status(200).json({
            status: 200,
            message: "Product Added Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
router.get("/all", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const result = yield product_1.default.getAllProducts();
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Product Data Fetched Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
router.post("/sort", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { sortType } = req.body;
        if (!sortType) {
            throw {
                statusCode: 400,
                customMessage: `All Parameters are required`,
            };
        }
        const result = yield product_1.default.getProductsBasedOnSortType(sortType);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Product Data Fetched Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
router.post("/details", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { productId, addView } = req.body;
        const result = yield product_1.default.getProductById(productId, addView);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        res.status(200).json({
            status: 200,
            data: result.doesProductExists.data,
            message: "Product Data Fetched Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
router.put("/update", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { productId, productName, price, liabilityPrice, images, sellerName, productAvailability, productDetails, isPublished, rentalDuration, bufferPeriod, } = req.body;
        const data = {
            productId,
            productName,
            price,
            liabilityPrice,
            images,
            sellerName,
            productAvailability,
            productDetails,
            isPublished,
            rentalDuration,
            bufferPeriod,
        };
        if (!productId) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required`,
            };
        }
        const result = yield product_1.default.updateProductInfo(data);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Product Data Fetched Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
router.delete("/:productId", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { productId } = req.params;
        const result = yield product_1.default.deleteProduct(productId);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Product Deleted Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
router.put("/status", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { productId, isPublished } = req.body;
        if (!productId) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required`,
            };
        }
        const result = yield product_1.default.updateProductPublishStatus({
            productId,
            isPublished,
        });
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Product Status Updated Successfully Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
router.get("/category/:categoryId", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { categoryId } = req.params;
        const result = yield product_1.default.getProductByCategory(categoryId);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Product Data Fetched Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
router.get("/sub-category/:subCategoryId", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { subCategoryId } = req.params;
        const result = yield product_1.default.getProductBySubCategory(subCategoryId);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Product Data Fetched Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
router.post("/user", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { accountId, isPublished } = req.body;
        const result = yield product_1.default.getProductByAccountId(accountId, isPublished);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Product Data Fetched Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
router.post("/stats", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { accountId } = req.body;
        const result = yield product_1.default.getUserProductStats(accountId);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Product Data Fetched Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
router.post("/filter", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { minPrice, maxPrice } = req.body;
        const result = yield product_1.default.getProductsByFilter(minPrice, maxPrice);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Product Data Fetched Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
router.put("/rental-status", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { productId, onRent } = req.body;
        if (!productId) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required`,
            };
        }
        const result = yield product_1.default.updateProductRentalStatus(productId, onRent);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Product Rental Status Updated Successfully Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
exports.default = router;
//# sourceMappingURL=product.js.map