"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const notification_1 = __importDefault(require("../controllers/notification"));
// Creating a router instance using express
const router = express_1.default.Router();
/**
 * @route /api/notification/mobile
 * @description
 *  - sends status 200 and a message 'Success' in response
 *
 */
router.post("/mobile", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { renteeName, orderId, sellerName, price, template, token, productName, } = req.body;
        // calls the send notification controller which sends the SMS and email based on the details provided
        const result = yield notification_1.default.sendMobileNotification({
            template,
            templateVariables: {
                renteeName,
                orderId,
                sellerName,
                price,
                productName,
            },
            token,
        });
        // If both notification type fails then throws an error
        if (result.isError) {
            throw result.error;
        }
        // Else send response to the client about the otp message
        res.status(200).send({
            message: "Notification sent successfully",
            status: 200,
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route /api/notification/email
 * @description
 *  - sends status 200 and a message 'Success' in response
 *
 */
router.post("/email", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { renteeName, orderId, sellerName, price, emailID, template, productName, } = req.body;
        // calls the send notification controller which sends the SMS and email based on the details provided
        const { email } = yield notification_1.default.sendMailNotification({
            emailID,
            template,
            templateVariables: {
                renteeName,
                orderId,
                sellerName,
                price,
                productName,
            },
        });
        // If both notification type fails then throws an error
        if (!email.success) {
            throw {};
        }
        // Else send response to the client about the otp message
        res.status(200).send({
            message: "Notification sent successfully",
            status: 200,
        });
    }
    catch (err) {
        next(err);
    }
}));
exports.default = router;
//# sourceMappingURL=notification.js.map