"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const blockchain_1 = require("../controllers/blockchain");
const api_1 = require("../helpers/api");
const big_js_1 = __importDefault(require("big.js"));
const nearApi = require("near-api-js");
const fs_1 = __importDefault(require("fs"));
const blockchain_2 = require("../controllers/blockchain");
const axios_1 = __importDefault(require("axios"));
const transactions_1 = require("../repositories/transactions");
const { decode } = require("bs58");
const orders_1 = __importDefault(require("../controllers/orders"));
const orders_2 = require("../repositories/orders");
const router = (0, express_1.Router)();
/**
 * @description This route is used to call a view contract method.
 * @requires
 * contract - Name of the contract in which the view method is called
 * method - Name of the method in smart contract
 * params - Parameters passed to the method
 */
router.post("/view", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { contract, method, params } = req.body;
        const result = yield (0, blockchain_1.viewContract)(contract, method, params);
        (0, api_1.ok)(req, res, result);
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @description This endpoint is used to call change methods in smart contract
 * @requires
 * account_id : Account id calling the contract
 * private_key : Private key of account_id
 * contract - Name of the contract in which the view method is called
 * method - Name of the method in smart contract
 * params - Parameters passed to the method
 * attached_token - attached token passed with the method if any
 * attached_gas
 *
 */
router.post("/call", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { account_id, private_key, attached_tokens, attached_gas, contract, method, params, } = req.body;
        const result = yield (0, blockchain_1.callContract)(account_id, private_key, attached_tokens, attached_gas, contract, method, params);
        (0, api_1.ok)(req, res, result);
    }
    catch (err) {
        next(err);
    }
}));
router.post("/transferFromImplicitAccount", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { account_id, private_key, amount, } = req.body;
        const keyPair = nearApi.utils.KeyPair.fromString(private_key);
        const keyStore = new nearApi.keyStores.InMemoryKeyStore();
        keyStore.setKey("testnet", account_id, keyPair);
        const near = yield nearApi.connect({
            networkId: "testnet",
            keyStore,
            masterAccount: account_id,
            nodeUrl: "https://rpc.testnet.near.org",
        });
        const account = yield near.account(account_id);
        const result = yield account.sendMoney("aqsus2.testnet", amount);
        (0, api_1.ok)(req, res, result);
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @description This endpoint is used to generate a link drop
 * @requires
 * account_id : Account id calling the contract
 * private_key : Private key of account_id
 * amount : Amount to be attached with the link drop . Amount should be greater than 1 near.
 *
 */
router.post("/generate-drop", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    try {
        const { account_id, private_key, amount } = req.body;
        // create a random key
        let newKeyPair = nearApi.KeyPair.fromRandom("ed25519");
        const public_key = (newKeyPair.public_key = newKeyPair.publicKey
            .toString()
            .replace("ed25519:", ""));
        // attach gas
        const DROP_GAS = (0, big_js_1.default)(3)
            .times(Math.pow(10, 13))
            .toFixed();
        // attach amount
        const amountInNear = yield nearApi.utils.format.parseNearAmount(amount);
        newKeyPair.amount = amount;
        newKeyPair.ts = Date.now();
        const result = yield (0, blockchain_1.callLinkDrop)(account_id, private_key, public_key, DROP_GAS, amountInNear);
        // if link drop contract is successful return key pair
        if ((_a = result === null || result === void 0 ? void 0 : result.status) === null || _a === void 0 ? void 0 : _a.hasOwnProperty("SuccessValue")) {
            return (0, api_1.ok)(req, res, {
                success: true,
                newKeyPair,
            });
        }
        (0, api_1.ok)(req, res, {
            success: false,
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @description Initializes the repo with master account
 * @requires
 * master_account_id : Account id with which this repo will be initialized
 * seed_phrase : Seed phrase of master account
 * private_key :private key of master account
 * nft_contract : default set to master_account_id
 * server_host : default set to localhost
 * server_port :default set to 3000
 * rpc_node : https://rpc.testnet.near.org
 */
router.post("/init", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const settings = yield JSON.parse(fs_1.default.readFileSync(api_1.CONFIG_PATH, "utf8"));
        if (settings.init_disabled) {
            throw new Error("Method now allowed");
        }
        let { master_account_id, seed_phrase, private_key, nft_contract, server_host, server_port, rpc_node, } = req.body;
        if (seed_phrase)
            private_key = (yield (0, blockchain_2.getKeysFromSeedPhrase)(seed_phrase)).secretKey;
        const result = yield (0, blockchain_1.initContract)(master_account_id, private_key, nft_contract, server_host, server_port, rpc_node);
        (0, api_1.ok)(req, res, result);
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @description This endpoint is used to create an account with near
 * @requires
 * name : Name of account to be created with near platform.
 *
 */
router.post("/create_user1", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const settings = yield JSON.parse(fs_1.default.readFileSync(api_1.CONFIG_PATH, "utf8"));
        const name = (req.body.name +
            "." +
            settings.master_account_id).toLowerCase();
        const data = {
            account_id: name,
            private_key: req.body.private_key,
            public_key: req.body.public_key,
            hex_key: req.body.hex_key,
            amount: req.body.amount,
        };
        const isNewAccountCreated = yield (0, blockchain_2.createAccount)(data);
        return (0, api_1.ok)(req, res, {
            publicKey: `{"accountId":"${name}","allKeys":["${data.public_key}"]}`,
            accountName: `${req.body.name}`,
            privateKey: `${data.private_key}`,
            text: `Account ${name} created. `,
        });
        (0, api_1.ok)(req, res, { text: "Error" });
    }
    catch (err) {
        next(err);
    }
}));
router.post("/create_user", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const settings = yield JSON.parse(fs_1.default.readFileSync(api_1.CONFIG_PATH, "utf8"));
        const name = (req.body.name +
            "." +
            settings.master_account_id).toLowerCase();
        // const url =
        //   "https://helper.testnet.near.org/account/walletState/gulaaaaam.testnet";
        // const result: any = await axios.get(url);
        const account = yield (0, blockchain_2.createKeyPair)(name);
        const data = {
            account_id: account.account_id,
            public_key: account.public_key,
            private_key: account.private_key,
        };
        return (0, api_1.ok)(req, res, {
            publicKey: `{"accountId":"${name}","allKeys":["${account.public_key}"]}`,
            accountName: `${req.body.name}`,
            privateKey: `${account.private_key}`,
            passPhrase: `${account.passPhrase}`,
            text: `Account ${name} created. `,
        });
    }
    catch (err) {
        next(err);
    }
}));
router.post("/create_mainnet_account", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const name = (req.body.name + ".gulam02.near").toLowerCase();
        const seed_phrase = req.body.passpPhrase;
        const account = yield (0, blockchain_2.getKeysFromSeedPhrase)(req.body.passPhrase.toLowerCase());
        // const account = await createKeyPair(name);
        const data = {
            account_id: name,
            public_key: account.publicKey,
            private_key: account.secretKey,
            seed_phrase: account.passPhrase,
            hex_key: req.body.hex_key,
            amount: req.body,
        };
        const isAccountCreated = yield (0, blockchain_2.createAccount)(data);
        if (isAccountCreated) {
            return (0, api_1.ok)(req, res, {
                publicKey: `{"accountId":"${name}","allKeys":["${data.public_key}"]}`,
                accountName: `${name}`,
                privateKey: `${data.private_key}`,
                text: `Account ${name} created. `,
            });
        }
        else
            (0, api_1.ok)(req, res, { text: "Error" });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @description Gets user keys from passphrase
 * @requires
 * passphrase - twelve word passphrase given at time of account creation.
 *
 */
router.post("/user_details", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const account = yield (0, blockchain_2.getKeysFromSeedPhrase)(req.body.seed_phrase.toLowerCase());
        let accountId;
        let doesAccountExist = true;
        const URL = "https://helper.testnet.near.org/publicKey/" +
            account.publicKey +
            "/accounts";
        try {
            const result = yield axios_1.default.get(URL);
            console.log("%c 🍺 result.data[0]: ", "font-size:20px;background-color: #42b983;color:#fff;", result.data[0]);
            if (!result.data[0]) {
                doesAccountExist = false;
            }
            else {
                if (result.data[0].length == 64) {
                    accountId = result.data[1];
                }
                else {
                    accountId = result.data[0];
                }
            }
        }
        catch (err) {
            doesAccountExist = false;
        }
        if (!doesAccountExist) {
            return (0, api_1.ok)(req, res, {
                text: "Error fetching account details",
                success: false,
            });
        }
        (0, api_1.ok)(req, res, {
            accountId: `${accountId}`,
            publicKey: `${account.publicKey}`,
            accountName: accountId,
            privateKey: `${account.secretKey}`,
            success: true,
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @description This endpoint is used to return balance of an account
 *  @param
 * account_id : Account id of the user
 *
 */
router.get("/balance/:accountId", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const balance = yield (0, blockchain_1.getBalance)(req.params.accountId);
        const balanceInNear = yield nearApi.utils.format.formatNearAmount(balance);
        (0, api_1.ok)(req, res, balanceInNear.slice(0, 5));
    }
    catch (err) {
        next(err);
    }
}));
router.get("/keypair", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const result = yield (0, blockchain_2.generateKeyPair)();
        (0, api_1.ok)(req, res, result);
    }
    catch (err) {
        next(err);
    }
}));
router.post("/parse_seed_phrase", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const account = yield (0, blockchain_2.getKeysFromSeedPhrase)(req.body.seed_phrase.toLowerCase());
        const hexPublicKey = decode(account.publicKey.replace("ed25519:", "")).toString("hex");
        account.hexPublicKey = hexPublicKey;
        (0, api_1.ok)(req, res, account);
    }
    catch (err) {
        next(err);
    }
}));
/**
   * @description This endpoint is used to reward users with near tokens once a user completes a video
   * @requires
   * account_id : Account id calling the contract
   * rewardTokenAmount : Amount in near to be given as reward to the user
   * private_key : private key of user
   * viewResourceParamObject : Params for checking if user has watched video
   * @example
   *  "viewResourceParamObject": {
            "method": "checkUserVideoWatchHistory",
            "params": {"mainAccount": "testAccount.testnet", "videoId": "1115"}
    },
   * writeResourceParamObject : Params for saving users video id
   * @example
   * "writeResourceParamObject": {
            "method": "saveUserVideoDetails",
            "params": {"mainAccount": "testAccount.testnet", "videoId": "1115"}
          }
   *
   */
router.post("/reward", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const settings = yield JSON.parse(fs_1.default.readFileSync(api_1.CONFIG_PATH, "utf8"));
        const { accountId, rewardTokenAmount, viewResourceParamObject, writeResourceParamObject, } = req.body;
        const isResourceUtilized = yield (0, blockchain_1.viewContract)(settings.master_account_id, viewResourceParamObject.method, viewResourceParamObject.params);
        if (!isResourceUtilized) {
            const amountInYocto = nearApi.utils.format.parseNearAmount(rewardTokenAmount);
            const rewardTransferred = yield (0, blockchain_1.callContract)(settings.master_account_id, settings.master_key, "", "", settings.nft_contract, "sendToken", { yoctonearAsU128: amountInYocto, walletAddress: accountId });
            yield (0, blockchain_1.callContract)(settings.master_account_id, settings.master_key, "", "", settings.nft_contract, writeResourceParamObject.method, writeResourceParamObject.params);
            return (0, api_1.ok)(req, res, rewardTransferred);
        }
        (0, api_1.ok)(req, res, `User has already utilized the resource`);
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @description This endpoint is used to return the public key used by the server
 */
router.get("/server_public_key", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const publicKey = req.app.locals.getPublicKey();
        (0, api_1.ok)(req, res, { publicKey });
    }
    catch (err) {
        next(err);
    }
}));
router.post("/format_amount", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { amount } = req.body;
        const amountInNear = yield nearApi.utils.format.parseNearAmount(amount);
        (0, api_1.ok)(req, res, { amountInNear });
    }
    catch (err) {
        next(err);
    }
}));
router.post("/create_holding_account", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const settings = yield JSON.parse(fs_1.default.readFileSync(api_1.CONFIG_PATH, "utf8"));
        const uniqueAccountId = req.body.uniqueAccountId;
        const senderPrivateKey = req.body.senderPrivateKey;
        const orderId = req.body.orderId;
        const transactionType = "DEBIT";
        const transactionDesc = "Paid";
        const senderAccountId = req.body.senderName;
        var amount = nearApi.utils.format.parseNearAmount(req.body.amount);
        var holdingAccountAmount = yield nearApi.utils.format.parseNearAmount("1");
        const name = (uniqueAccountId +
            "." +
            settings.master_account_id).toLowerCase();
        const account = yield (0, blockchain_2.createKeyPair)(name);
        const data = {
            account_id: name,
            private_key: account.private_key,
            public_key: account.public_key,
            amount: holdingAccountAmount,
            hex_key: "",
        };
        //create account
        const isAccountCreated = yield (0, blockchain_2.createAccount)(data);
        if (isAccountCreated) {
            // if account is created
            const DROP_GAS = (0, big_js_1.default)(3)
                .times(Math.pow(10, 13))
                .toFixed();
            const result = yield (0, blockchain_1.sendAmount)(req.body.senderName, name, senderPrivateKey, amount);
            var transactionData = {
                orderId,
                amount: req.body.amount,
                transactionType,
                transactionDesc,
                accountId: senderAccountId,
            };
            // add transaction
            let transactionResult = yield (0, transactions_1.addTransaction)(transactionData);
            // if add transaction passes
            if (transactionResult.success == true) {
                let seedPhraseData = {
                    seedPhrase: `${account.passPhrase}`,
                    orderId: orderId,
                    holdingAccountId: name,
                };
                console.log("%c 🌰 seedPhraseData: ", "font-size:20px;background-color: #EA7E5C;color:#fff;", seedPhraseData);
                // update seed phrase in db
                let updateSeedPhrase = yield orders_1.default.updateSeedPhraseForOrder(seedPhraseData);
                // if update seed phrase fails
                if (updateSeedPhrase.isError == true) {
                    const result = yield (0, blockchain_1.deleteHoldingAccount)(name, "aqsus2.testnet", account.private_key);
                    return (0, api_1.ok)(req, res, {
                        text: "Failed to save passphrase in database",
                    });
                }
            }
            else {
                //delete holding account
                const result = yield (0, blockchain_1.deleteHoldingAccount)(name, senderAccountId, account.private_key);
                return (0, api_1.ok)(req, res, { text: "Transaction result couldn't be saved" });
            }
            return (0, api_1.ok)(req, res, {
                publicKey: `{"accountId":"${name}","allKeys":["${account.public_key}"]}`,
                accountName: `${name}`,
                privateKey: `${account.private_key}`,
                passPhrase: `${account.passPhrase}`,
                text: `Account ${name} created. `,
            });
        }
        else
            (0, api_1.ok)(req, res, { text: "Holding Account couldn't be created" });
    }
    catch (err) {
        console.log("%c 🥒 err: ", "font-size:20px;background-color: #4b4b4b;color:#fff;", err);
        next(err);
    }
}));
router.post("/deleteImplicit", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const account = yield (0, blockchain_1.getMasterAccount)();
        const result = account.deleteAccount(req.body.hex_key);
        return (0, api_1.ok)(req, res, {
            result,
        });
    }
    catch (err) {
        next(err);
    }
}));
router.post("/deleteHoldingAccount", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { holderAccountId, holdingAccountPrivateKey, receiverAccountId } = req.body;
        const result = yield (0, blockchain_1.deleteHoldingAccount)(holderAccountId, receiverAccountId, holdingAccountPrivateKey);
        return (0, api_1.ok)(req, res, {
            result,
        });
    }
    catch (err) {
        next(err);
    }
}));
router.post("/transferToRenter", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { orderId, renterAccountId } = req.body;
        const transactionType = "CREDIT";
        const transactionDesc = "Received";
        let orderDetails = yield orders_1.default.fetchOrderByOrderId(orderId);
        const account = yield (0, blockchain_2.getKeysFromSeedPhrase)(orderDetails.data.seedPhrase.toLowerCase());
        const totalPrice = (orderDetails.data.productPrice + orderDetails.data.shippingPrice).toString();
        const amountInYocto = yield nearApi.utils.format.parseNearAmount(totalPrice);
        const result = yield (0, blockchain_1.sendAmount)(orderDetails.data.holdingAccountId, renterAccountId, account.secretKey, amountInYocto);
        var transactionData = {
            orderId,
            amount: totalPrice,
            transactionType,
            transactionDesc,
            accountId: renterAccountId,
        };
        // add transaction
        let transactionResult = yield (0, transactions_1.addTransaction)(transactionData);
        return (0, api_1.ok)(req, res, {
            result,
        });
    }
    catch (err) {
        next(err);
    }
}));
router.post("/cancel-approved-order", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { orderId, renteeAccountId } = req.body;
        //get order details by order id
        let orderDetails = yield orders_1.default.fetchOrderByOrderId(orderId);
        // get account
        let accountKeys = yield (0, blockchain_2.getKeysFromSeedPhrase)(orderDetails.data.seedPhrase.toLowerCase());
        const result = yield (0, blockchain_1.deleteHoldingAccount)(orderDetails.data.holdingAccountId, renteeAccountId, accountKeys.secretKey);
        if (result["status"].hasOwnProperty("SuccessValue")) {
            const updateOrderData = {
                orderId,
                orderStatus: "Cancelled",
            };
            const updateStatus = yield (0, orders_2.updateOrderStatus)(updateOrderData);
            var transactionData = {
                orderId,
                amount: req.body.amount,
                transactionType: "CREDIT",
                transactionDesc: "Order Cancelled",
                accountId: renteeAccountId,
            };
            // add transaction
            let transactionResult = yield (0, transactions_1.addTransaction)(transactionData);
            return (0, api_1.ok)(req, res, {
                result,
            });
        }
        else {
            return (0, api_1.ok)(req, res, { text: "Order couldn't be cancelled" });
        }
    }
    catch (err) {
        console.log(err);
        next(err);
    }
}));
router.post("/return-order", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { orderId, renteeAccountId, renterAccountId, isDamaged } = req.body;
        //get order details by order id
        let orderDetails = yield orders_1.default.fetchOrderByOrderId(orderId);
        // get account
        let accountKeys = yield (0, blockchain_2.getKeysFromSeedPhrase)(orderDetails.data.seedPhrase.toLowerCase());
        if (isDamaged) {
            var result = yield (0, blockchain_1.deleteHoldingAccount)(orderDetails.data.holdingAccountId, renterAccountId, accountKeys.secretKey);
        }
        else {
            var result = yield (0, blockchain_1.deleteHoldingAccount)(orderDetails.data.holdingAccountId, renteeAccountId, accountKeys.secretKey);
        }
        if (result["status"].hasOwnProperty("SuccessValue")) {
            const updateOrderData = {
                orderId,
                orderStatus: "Returned",
            };
            var transactionData = {
                orderId,
                amount: orderDetails.data.liabilityPrice,
                transactionType: "CREDIT",
                transactionDesc: "Liability received",
                accountId: renteeAccountId,
            };
            let transactionResult = yield (0, transactions_1.addTransaction)(transactionData);
            (0, api_1.ok)(req, res, result);
        }
        else {
            return (0, api_1.ok)(req, res, {
                text: "Failed to delete holding account while returning product",
            });
        }
    }
    catch (err) {
        console.log(err);
        next(err);
    }
}));
exports.default = router;
//# sourceMappingURL=blockchain.js.map