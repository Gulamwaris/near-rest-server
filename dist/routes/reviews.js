"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const reviews_1 = __importDefault(require("../controllers/reviews"));
const router = express_1.default.Router();
/**
 * @route for fetching reviews by productId
 *  * @description
 *  - Returns reviews for the productId passed
 */
router.get("/:productId", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { productId } = req.params;
        //if any of the below parameters is missing, return an error response
        if (!productId) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required.
          ${!productId && "Id is required."}`,
            };
        }
        const result = yield reviews_1.default.fetchAllReviewByProductId(productId);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Fetched Reviews Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
/**
 * @route for storing review details in DB
 *  * @description
 *  - Takes productId, reviewTitle, reviewDesc, postedBy, rating
 *  - Stores the review details in DB for a product
 * @params
 *  - @requires productId Product Id
 *  - @requires reviewDesc Review Desc
 *  - @requires reviewTitle Review Title
 *  - @requires postedBy Name of the user who posted the review
 *  - @requires rating No of stars
 */
router.post("/", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Get required fields from the req body
        const { productId, reviewTitle, reviewDesc, postedBy, rating, orderId } = req.body;
        //if any of the below parameters is missing, return an error response
        if (!productId) {
            throw {
                statusCode: 400,
                customMessage: `All parameters are required.
          ${!productId && "Id is required."}`,
            };
        }
        const reviewDetails = {
            productId,
            reviewTitle,
            reviewDesc,
            postedBy,
            rating,
            orderId,
        };
        // Call the updateProfile controller function
        const result = yield reviews_1.default.addReview(reviewDetails);
        // if there is error the throw the error
        if (result.isError) {
            throw result.error;
        }
        // return success response
        res.status(200).json({
            status: 200,
            data: result.data,
            message: "Review Added Successfully",
        });
    }
    catch (err) {
        next(err);
    }
}));
exports.default = router;
//# sourceMappingURL=reviews.js.map