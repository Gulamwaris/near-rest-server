"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const update_delivery_details_1 = __importDefault(require("../scripts/update-delivery-details"));
const logger_1 = __importDefault(require("../utils/logger"));
// Creating a router instance using express
const router = express_1.default.Router();
router.get("/delivery-info", (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // Call the script to fetch all upcoming dues
        yield (0, update_delivery_details_1.default)();
        // Return a positive response to the client
        res.status(200).json({
            status: 200,
            message: "Ddetails updated successfully",
        });
    }
    catch (err) {
        logger_1.default.error(`at: "routes/scripts/delivery-info ${JSON.stringify(err)}\n${err}`);
        next(err);
    }
}));
exports.default = router;
//# sourceMappingURL=scripts.js.map