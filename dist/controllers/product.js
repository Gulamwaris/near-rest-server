"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Controller for products - Bhavana
const logger_1 = __importDefault(require("../utils/logger"));
const productsRepositories = __importStar(require("../repositories/product"));
// Controller for adding a new product - Bhavana
const addProduct = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // calling the createProduct repo with the product data
        const result = yield productsRepositories.addProduct(data);
        // If there is any error then throw the error
        if (!result.success) {
            throw {
                statusCode: 400,
                customMessage: result.errorMessage,
            };
        }
        // If there is no error then return error as false
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/products/addProduct" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for updating a product details by productId - Bhavana
const updateProductInfo = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // calling the updateProductInfo repo with the product data
        const result = yield productsRepositories.updateProductInfo(data);
        // If there is any error then throw the error
        if (!result.success) {
            throw {
                statusCode: 400,
                customMessage: result.errorMessage,
            };
        }
        // If there is no error then return error as false
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/products/updateProductInfo" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for updating product publish status - Bhavana
const updateProductPublishStatus = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // calling the updateProductPublishStatus repo with the product data
        const result = yield productsRepositories.updateProductPublishStatus(data.productId, data.isPublished);
        // If there is any error then throw the error
        if (!result.success) {
            throw {
                statusCode: 400,
                customMessage: result.errorMessage,
            };
        }
        // If there is no error then return error as false
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/products/updateProductPublishStatus" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for fethcing all products - Bhavana
const getAllProducts = () => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    try {
        // call the getAllProducts repo
        const productList = yield productsRepositories.getAllProducts();
        // if product not exists in db then throw an error
        if (!productList.success) {
            throw {
                statusCode: 400,
                customMessage: productList.errorMessage,
            };
        }
        // if product not exists in db then throw an error
        if (((_a = productList === null || productList === void 0 ? void 0 : productList.data) === null || _a === void 0 ? void 0 : _a.length) === 0) {
            throw {
                statusCode: 400,
                customMessage: "No Products found",
            };
        }
        // If there is no error then return error as false
        return {
            data: productList.data,
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/products/getAllProducts" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for fethcing products based on sort TYpe - Bhavana
const getProductsBasedOnSortType = (sortType) => __awaiter(void 0, void 0, void 0, function* () {
    var _b;
    try {
        // Call the getProduuctsBySortType to fetch the products
        const productList = yield productsRepositories.getProductsBySortType(sortType);
        // if product not exists in db then throw an error
        if (!productList.success) {
            throw {
                statusCode: 400,
                customMessage: productList.errorMessage,
            };
        }
        // if product not exists in db then throw an error
        if (((_b = productList === null || productList === void 0 ? void 0 : productList.data) === null || _b === void 0 ? void 0 : _b.length) === 0) {
            throw {
                statusCode: 400,
                customMessage: "No Products found",
            };
        }
        // If there is no error then return error as false
        return {
            data: productList.data,
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/products/getAllProducts" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for deleting a product by product Id - Bhavana
const deleteProduct = (productId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // Checking whther the product exists
        const isProductExist = yield productsRepositories.getProductById(productId);
        // If product doesn't exists then throw the error mmessage
        if (!isProductExist.success) {
            throw {
                statusCode: 400,
                customMessage: isProductExist.errorMessage,
            };
        }
        // call the repo to delete product
        const result = yield productsRepositories.deleteProduct(productId);
        // If there is any error then throw the error
        if (!result.success) {
            throw {
                statusCode: 400,
                customMessage: result.errorMessage,
            };
        }
        // If there is no error then return error as false
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/products/deleteProduct" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for fethcing products by productId - Bhavana
const getProductById = (productId, addView) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const doesProductExists = yield productsRepositories.getProductById(productId, addView);
        if (!doesProductExists.success) {
            throw {
                statusCode: 400,
                customMessage: doesProductExists.errorMessage,
            };
        }
        // If there is no error then return error as false
        return {
            isError: false,
            doesProductExists,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/products/getProductById" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for fethcing products by sub category - Bhavana
const getProductBySubCategory = (subCategoryId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const productList = yield productsRepositories.getProductBySubCategory(subCategoryId);
        // if product not exists in db then throw an error
        if (!productList.success) {
            throw {
                statusCode: 400,
                customMessage: productList.errorMessage,
            };
        }
        // If there is no error then return error as false
        return {
            data: productList.data,
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/products/getProductBySubCategory" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for fethcing products by category - Bhavana
const getProductByCategory = (categoryId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const productList = yield productsRepositories.getProductByCategory(categoryId);
        // if product not exists in db then throw an error
        if (!productList.success) {
            throw {
                statusCode: 400,
                customMessage: productList.errorMessage,
            };
        }
        // If there is no error then return error as false
        return {
            data: productList.data,
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/products/getProductByCategory" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for fethcing products by accountId based on published flag - Bhavana
const getProductByAccountId = (accountId, isPublished) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const productList = yield productsRepositories.getProductByAccountId(accountId, isPublished);
        // if product not exists in db then throw an error
        if (!productList.success) {
            throw {
                statusCode: 400,
                customMessage: productList.errorMessage,
            };
        }
        // If there is no error then return error as false
        return {
            data: productList.data,
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/products/getProductByAccountId" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for fethcing stats for all product based on acccountID - Bhavana
const getUserProductStats = (accountId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const productList = yield productsRepositories.getUserProductsStats(accountId);
        // if product not exists in db then throw an error
        if (!productList.success) {
            throw {
                statusCode: 400,
                customMessage: productList.errorMessage,
            };
        }
        // If there is no error then return error as false
        return {
            data: productList.data,
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/products/getUserProductStats" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for fethcing products BY FILTER APPLIES - Bhavana
const getProductsByFilter = (minPrice, maxPrice) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const productList = yield productsRepositories.getProductsByFilter(minPrice, maxPrice);
        // if product not exists in db then throw an error
        if (!productList.success) {
            throw {
                statusCode: 400,
                customMessage: productList.errorMessage,
            };
        }
        // If there is no error then return error as false
        return {
            data: productList.data,
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/products/getProductsByFilter" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for updating rental sttaus of product - Bhavana
const updateProductRentalStatus = (productId, status) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // calling the updateProductInfo repo with the product data
        const result = yield productsRepositories.updateRentalStatus(productId, status);
        // If there is any error then throw the error
        if (!result.success) {
            throw {
                statusCode: 400,
                customMessage: result.errorMessage,
            };
        }
        // If there is no error then return error as false
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/products/updateProductRentalStatus" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
exports.default = {
    addProduct,
    updateProductInfo,
    getAllProducts,
    deleteProduct,
    getProductById,
    getProductsBasedOnSortType,
    getProductBySubCategory,
    getProductByCategory,
    getProductByAccountId,
    getUserProductStats,
    updateProductPublishStatus,
    getProductsByFilter,
    updateProductRentalStatus,
};
//# sourceMappingURL=product.js.map