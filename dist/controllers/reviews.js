"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const reviewsRepositories = __importStar(require("../repositories/reviews"));
const productsRepositories = __importStar(require("../repositories/product"));
const logger_1 = __importDefault(require("../utils/logger"));
// Controller for adding a new review - Bhavana
const addReview = (data) => __awaiter(void 0, void 0, void 0, function* () {
    var _a;
    try {
        const result = yield reviewsRepositories.addProductReview(data);
        let totalReviews;
        let rating;
        // If there is any error then throw the error
        if (!result.success) {
            throw {
                statusCode: 400,
                customMessage: "Unable to save review Details in DB",
            };
        }
        // Fetching product Info based on product id for updating ratings
        const productInfo = yield productsRepositories.getProductById(data.productId);
        // Storing it in variable
        rating = (_a = productInfo === null || productInfo === void 0 ? void 0 : productInfo.data) === null || _a === void 0 ? void 0 : _a.rating;
        // Updating total ratings
        rating.totalRatings = rating.totalRatings + parseInt(data.rating);
        // Fetching the reviews of a product
        const reviewDetails = yield reviewsRepositories.fetchReviewByProductId(data.productId);
        // Storing no of reviews in a variable
        totalReviews = reviewDetails.data.reviews.length;
        // calculate average rating in a variable
        rating.averageRatings = rating.totalRatings / totalReviews;
        // UPdating the product rating in the products collection
        yield productsRepositories.updateProductRatings(data.productId, rating);
        // If there is no error then return error as false
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/reviews/addReview" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for fetching all reviews by product ID - Bhavana
const fetchAllReviewByProductId = (productId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Call the repository to fetch the user
        const result = yield reviewsRepositories.fetchReviewByProductId(productId);
        // Checks for the exact result value
        if (!result.data) {
            return {
                isError: false,
                data: [],
            };
        }
        return {
            isError: false,
            data: result.data.reviews,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/reviews/fetchAllReviewByProductId" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
exports.default = {
    addReview,
    fetchAllReviewByProductId,
};
//# sourceMappingURL=reviews.js.map