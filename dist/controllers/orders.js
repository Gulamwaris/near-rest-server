"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const ordersRepositories = __importStar(require("../repositories/orders"));
const productsRepositories = __importStar(require("../repositories/product"));
const userRepositories = __importStar(require("../repositories/users"));
const notification_1 = __importDefault(require("./notification"));
const logger_1 = __importDefault(require("../utils/logger"));
// Controller for saving a new order - Bhavana
const saveOrder = (data) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b, _c, _d;
    try {
        // Call the repo to save the order
        const result = yield ordersRepositories.saveOrder(data);
        // If there is any error then throw the error
        if (!result.success) {
            throw {
                statusCode: 400,
                customMessage: "Unable to place Order",
            };
        }
        // call the notification controller to sedn the notification regarding oreder placeed to renter
        const notificationResult = yield orderNotifications(data.renterAccountId, "order-placed", {
            orderId: (_a = result === null || result === void 0 ? void 0 : result.data) === null || _a === void 0 ? void 0 : _a._id,
            productName: (_b = result === null || result === void 0 ? void 0 : result.data) === null || _b === void 0 ? void 0 : _b.productName,
            sellerName: (_c = result === null || result === void 0 ? void 0 : result.data) === null || _c === void 0 ? void 0 : _c.sellerName,
        });
        // If both notification type fails then throws an error
        if (notificationResult.isError) {
            logger_1.default.info(`Failed to send notification for order with orderId ${(_d = result === null || result === void 0 ? void 0 : result.data) === null || _d === void 0 ? void 0 : _d._id}`);
        }
        // If thre is no error then return error as false
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/orders/saveOrder" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for updating order status  - Bhavana
const updateOrderStatus = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        var templateVaribles;
        // Call the repository to fetch the user
        const result = yield ordersRepositories.updateOrderStatus(data);
        // Checks for the exact result value
        if (!result) {
            return {
                isError: false,
                data: `Unable to order status for order with ID : ${data.orderId}`,
            };
        }
        // Fethcing the order by order Id
        const orderResult = yield ordersRepositories.getOrderByOrderId(data.orderId);
        // Storing orderDetails in a varaible
        const orderDetails = orderResult.data;
        // fetching the user details by accountId
        const userInfo = yield userRepositories.fetchUserByAccountId(orderDetails.renteeAccountId);
        if (userInfo.data) {
            templateVaribles = {
                orderId: data.orderId,
                price: orderDetails.totalPrice,
                sellerName: orderDetails.sellerName,
                renteeName: userInfo.data.firstName,
            };
        }
        // Perform action son order Status based on different status received
        switch (data.orderStatus) {
            // If it is approved send the approval notification to rentee
            case "Approved":
                yield sendNotification("approve-order");
                break;
            // If it is rejected send the rejection notification to rentee
            case "Rejected":
                yield sendNotification("reject-order");
                break;
            // If order is paid send the paymnet notification to renter
            case "Paid":
                templateVaribles = {
                    orderId: data.orderId,
                    price: orderDetails.totalPrice,
                    sellerName: orderDetails.sellerName,
                    productName: orderDetails.productName,
                };
                yield sendNotification("payment");
                break;
            // If it is delivered send the delivery notification to rentee
            case "Delivered":
                yield sendNotification("order-delivered");
            default:
                break;
        }
        // Send notification function for sedning notification
        function sendNotification(template) {
            return __awaiter(this, void 0, void 0, function* () {
                yield orderNotifications(orderDetails.renteeAccountId, template, templateVaribles);
            });
        }
        // If thre is no error then return true
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/orders/updateOrderStatus" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for fethcing order by order ID  - Bhavana
const fetchOrderByOrderId = (orderId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Call the repository to fetch the getOrderByOrderId
        const result = yield ordersRepositories.getOrderByOrderId(orderId);
        // Checks for the exact result value
        if (!result.success) {
            throw {
                statusCode: 400,
                customMessage: `Order Not Found With id: ${orderId}`,
            };
        }
        return {
            isError: false,
            data: result.data,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/orders/fetchOrderByOrderId" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for fethcing rentee orders   - Bhavana
const fetchRenteeOrderByOrderId = (accountId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Call the repository to fetch the all the order of a rentee
        const result = yield ordersRepositories.getRenteeOrdersByAccountId(accountId);
        // Checks for the exact result value
        if (!result.success) {
            throw {
                statusCode: 400,
                customMessage: `Order Not Found for id: ${accountId}`,
            };
        }
        return {
            isError: false,
            data: result.data,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/orders/fetchRenteeOrderByOrderId" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for fethcing renter orders - Bhavana
const fetchRenterOrderByOrderId = (accountId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Call the repository to fetch all the renter orders
        const result = yield ordersRepositories.getRenterOrdersByAccountId(accountId);
        // Checks for the exact result value
        if (!result.success) {
            throw {
                statusCode: 400,
                customMessage: `Order Not Found for id: ${accountId}`,
            };
        }
        return {
            isError: false,
            data: result.data,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/orders/fetchRenterOrderByOrderId" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for fethcing all the orders for a particluar user and status - Bhavana
const fetchOrderByOrderStatus = (status, accountId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Call the repository to getOrdersByStatus
        const result = yield ordersRepositories.getOrdersByStatus(status, accountId);
        // Checks for the exact result value
        if (!result.success) {
            throw {
                statusCode: 400,
                customMessage: `Order Not Found With status: ${status}`,
            };
        }
        return {
            isError: false,
            data: result.data,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/orders/fetchOrderByOrderStatus" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller to fetch pending orders for a user - Bhavana
const fetchPendingOrders = (accountId) => __awaiter(void 0, void 0, void 0, function* () {
    var _e, _f, _g, _h, _j, _k;
    try {
        //Call the repository to fetch pedning orders
        const ordersData = yield ordersRepositories.getPendingOrders(accountId);
        // Checks for the exact result value
        if (!ordersData.success) {
            throw {
                statusCode: 400,
                customMessage: `Order Not Found With Pending Status`,
            };
        }
        // Store the details in a vraible
        const orderDetails = ordersData.data;
        let result = [];
        // Getting all unique productIds of pending orders
        const productIds = orderDetails
            .map((item) => item.productId)
            .filter((value, index, self) => self.indexOf(value) === index);
        // looping through the productIds array
        for (let i = 0; i < productIds.length; i++) {
            // Decalring empty object
            let singleProduct = { orderDetails: [] };
            // Call the repo for fetching products by productId
            const product = yield productsRepositories.getProductById(productIds[i]);
            // Storing productDetails
            singleProduct.productName = (_e = product === null || product === void 0 ? void 0 : product.data) === null || _e === void 0 ? void 0 : _e.productName;
            singleProduct.productPrice = (_f = product === null || product === void 0 ? void 0 : product.data) === null || _f === void 0 ? void 0 : _f.price;
            singleProduct.rating = (_g = product === null || product === void 0 ? void 0 : product.data) === null || _g === void 0 ? void 0 : _g.rating;
            singleProduct.productImage = (_h = product === null || product === void 0 ? void 0 : product.data) === null || _h === void 0 ? void 0 : _h.images[0];
            singleProduct.bufferDuration = ((_j = product === null || product === void 0 ? void 0 : product.data) === null || _j === void 0 ? void 0 : _j.bufferDuration)
                ? (_k = product === null || product === void 0 ? void 0 : product.data) === null || _k === void 0 ? void 0 : _k.bufferDuration
                : null;
            // Looping through orderDetails array
            for (let j = 0; j < orderDetails.length; j++) {
                // Comparing productIds
                if (productIds[i] == orderDetails[j].productId) {
                    singleProduct.orderDetails.push(orderDetails[j]);
                }
            }
            // Pushing the productDetails in an rray
            result.push(singleProduct);
        }
        return {
            isError: false,
            data: result,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/orders/fetchOrderByOrderStatus" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for sending notifications
const orderNotifications = (accountId, template, templateVariables) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // Call the mail notification
        yield notification_1.default.sendMailNotification({
            template: template,
            templateVariables: templateVariables,
            accountId,
        });
        // Call the mobile notification
        yield notification_1.default.sendMobileNotification({
            template: template,
            templateVariables: templateVariables,
            accountId,
        });
        return { isError: false };
    }
    catch (err) {
        logger_1.default.error(`at: "controllers/orders/orderNotifications", ${JSON.stringify(err)} \n ${err}`);
        return { isError: true, error: err };
    }
});
// Controller to update seed phrase for order - Bhavana
const updateSeedPhraseForOrder = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // Create a hash of the seedPhrase
        // data.seedPhrase = generateHash(data.seedPhrase, "SHA256");
        // Call the repository to update seed phrase of a order
        const result = yield ordersRepositories.updateSeedPhraseForOrder(data);
        // Checks for the exact result value
        if (!result) {
            return {
                isError: false,
                data: `Unable to order status for order with ID : ${data.orderId}`,
            };
        }
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/orders/updateSeedPhraseForOrder" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller to delete seed phrase of aorder based on orderId - Bhavana
const deleteSeedPhraseForOrder = (orderId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // Call the repository to delete seedphrase of a order
        const result = yield ordersRepositories.deleteSeedPhraseForOrder(orderId);
        // Checks for the exact result value
        if (!result) {
            return {
                isError: false,
                data: `Unable to delete seed phrase for order with ID : ${orderId}`,
            };
        }
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/orders/deleteSeedPhraseForOrder" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller to get seedphrase for a order - Bhavana
const getSeedPhraseForOrder = (orderId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // Call the repository to get orderDetails
        const result = yield ordersRepositories.getOrderByOrderId(orderId);
        // Checks for the exact result value
        if (!result) {
            return {
                isError: false,
                data: `Unable to delete seed phrase for order with ID : ${orderId}`,
            };
        }
        return {
            isError: false,
            data: result === null || result === void 0 ? void 0 : result.data,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/orders/getSeedPhraseForOrder" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller to update delivery details once order is delivered to rentee - Bhavana
const updateDeliveryDetails = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // Call the repository to update delivery details
        const result = yield ordersRepositories.updateDeliveryDetails(data);
        // Checks for the exact result value
        if (!result) {
            return {
                isError: false,
                data: `Unable to order status for order with ID : ${data.orderId}`,
            };
        }
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/orders/updateDeliveryDetails" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller to fupdate order return details once order is returend to renter - Bhavana
const updateOrderReturnDetails = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // Call the repository to update order return details
        const result = yield ordersRepositories.updateOrderReturnDetails(data);
        // Checks for the exact result value
        if (!result) {
            return {
                isError: false,
                data: `Unable to order return details for order with ID : ${data.orderId}`,
            };
        }
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/orders/updateOrderReturnDetails" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller to update dmage approval status by rentee - Bhavana
const updateDamageApprovedByRentee = (isApproved, orderId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // Call the repository to updateDamageApprovedByRentee
        const result = yield ordersRepositories.updateDamageApprovedByRentee(isApproved, orderId);
        // Checks for the exact result value
        if (!result) {
            return {
                isError: false,
                data: `Unable to order return details for order with ID : ${orderId}`,
            };
        }
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/orders/updateDamageApprovedByRentee" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
exports.default = {
    saveOrder,
    updateOrderStatus,
    fetchOrderByOrderId,
    fetchRenteeOrderByOrderId,
    fetchRenterOrderByOrderId,
    fetchOrderByOrderStatus,
    fetchPendingOrders,
    updateSeedPhraseForOrder,
    deleteSeedPhraseForOrder,
    getSeedPhraseForOrder,
    updateDeliveryDetails,
    updateOrderReturnDetails,
    updateDamageApprovedByRentee,
};
//# sourceMappingURL=orders.js.map