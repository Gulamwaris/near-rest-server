"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getKeysFromSeedPhrase = exports.createAccount = exports.createKeyPair = exports.generateKeyPair = exports.deleteHoldingAccount = exports.getAccountByKey = exports.getMasterAccount = exports.sendAmount = exports.callLinkDrop = exports.callContract = exports.getBalance = exports.initContract = exports.viewContract = void 0;
const nearApi = require("near-api-js");
const fs = require("fs");
const nearSeedPhrase = require("near-seed-phrase");
const axios_1 = __importDefault(require("axios"));
const api_1 = require("../helpers/api");
let settings;
// Check if near-api-server.config.json file exists
if (fs.existsSync(api_1.CONFIG_PATH)) {
    // if exists , take settings from the file
    settings = JSON.parse(fs.readFileSync(api_1.CONFIG_PATH, "utf8"));
}
else {
    const data = {
        server_host: "localhost",
        server_port: 3000,
        rpc_node: "https://rpc.testnet.near.org",
        allow_rpc_update: false,
    };
    // if file doesnt exists m create and write to file
    fs.writeFileSync(api_1.CONFIG_PATH, JSON.stringify(data), { flag: "wx" }, function (err) {
        if (err) {
            console.log(err);
        }
    });
    settings = JSON.parse(fs.readFileSync(api_1.CONFIG_PATH, "utf8"));
}
const viewContract = (recipient, method, params, rpc_node) => __awaiter(void 0, void 0, void 0, function* () {
    console.log("method: ", method, recipient);
    try {
        const nearRpc = new nearApi.providers.JsonRpcProvider(rpc_node || settings.rpc_node);
        const account = new nearApi.Account({ provider: nearRpc });
        const result = yield account.viewFunction(recipient, method, params);
        return result;
    }
    catch (e) {
        console.log("error in viewContract: ");
        console.log(e);
        return (0, api_1.reject)(e);
    }
});
exports.viewContract = viewContract;
const initContract = (master_account_id, master_key, nft_contract, server_host, server_port, rpc_node) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const new_settings = settings;
        if (master_account_id)
            new_settings.master_account_id = master_account_id;
        if (master_key)
            new_settings.master_key = master_key;
        if (nft_contract)
            new_settings.nft_contract = nft_contract;
        if (server_host)
            new_settings.server_host = server_host;
        if (server_port)
            new_settings.server_port = server_port;
        if (rpc_node)
            new_settings.rpc_node = rpc_node;
        yield fs.promises.writeFile(api_1.CONFIG_PATH, JSON.stringify(Object.assign({}, new_settings)));
        return (0, api_1.notify)("Settings updated.");
    }
    catch (e) {
        return (0, api_1.reject)(e);
    }
});
exports.initContract = initContract;
const getBalance = (account_id) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const body = {
            jsonrpc: "2.0",
            id: "dontcare",
            method: "query",
            params: {
                request_type: "view_account",
                finality: "final",
                account_id: account_id,
            },
        };
        const result = yield axios_1.default.post(settings.rpc_node, body);
        return result.data.result.amount;
    }
    catch (e) {
        return (0, api_1.reject)(e);
    }
});
exports.getBalance = getBalance;
const callContract = (account_id, private_key, attached_tokens, attached_gas, recipient, method, params) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const account = yield (0, exports.getAccountByKey)(account_id, private_key);
        return yield account.functionCall(recipient, method, params, attached_gas, attached_tokens);
    }
    catch (e) {
        return (0, api_1.reject)(e);
    }
});
exports.callContract = callContract;
const callLinkDrop = (account_id, private_key, public_key, GAS, amount) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const account = yield (0, exports.getAccountByKey)(account_id, private_key);
        return yield account.functionCall("testnet", "send", { public_key }, GAS, amount);
    }
    catch (e) {
        return (0, api_1.reject)(e);
    }
});
exports.callLinkDrop = callLinkDrop;
const sendAmount = (account_id, holderAccount, private_key, amount) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const account = yield (0, exports.getAccountByKey)(account_id, private_key);
        return yield account.sendMoney(holderAccount, amount);
    }
    catch (e) {
        console.log(e);
        return (0, api_1.reject)(e);
    }
});
exports.sendAmount = sendAmount;
const getMasterAccount = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const keyPair = nearApi.utils.KeyPair.fromString(settings.master_key);
        const keyStore = new nearApi.keyStores.InMemoryKeyStore();
        keyStore.setKey("testnet", settings.master_account_id, keyPair);
        const near = yield nearApi.connect({
            networkId: "testnet",
            keyStore,
            masterAccount: settings.master_account_id,
            nodeUrl: settings.rpc_node,
        });
        return yield near.account(settings.master_account_id);
    }
    catch (e) {
        return (0, api_1.reject)(e);
    }
});
exports.getMasterAccount = getMasterAccount;
const getAccountByKey = (account_id, private_key) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        private_key = private_key.replace('"', "");
        const keyPair = nearApi.utils.KeyPair.fromString(private_key);
        const keyStore = new nearApi.keyStores.InMemoryKeyStore();
        keyStore.setKey("testnet", account_id, keyPair);
        const near = yield nearApi.connect({
            networkId: "testnet",
            keyStore,
            masterAccount: account_id,
            nodeUrl: settings.rpc_node,
        });
        return yield near.account(account_id);
    }
    catch (e) {
        return (0, api_1.reject)(e);
    }
});
exports.getAccountByKey = getAccountByKey;
const deleteHoldingAccount = (holderAccountId, receiverAccountId, private_key) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const account = yield (0, exports.getAccountByKey)(holderAccountId, private_key);
        return yield account.deleteAccount(receiverAccountId);
    }
    catch (e) {
        return (0, api_1.reject)(e);
    }
});
exports.deleteHoldingAccount = deleteHoldingAccount;
// Generates random public private key pair using fromRandom function defined within near utils
const generateKeyPair = () => __awaiter(void 0, void 0, void 0, function* () {
    const keypair = nearApi.utils.KeyPair.fromRandom("ed25519");
    return {
        public_key: keypair.publicKey.toString(),
        private_key: keypair.secretKey,
    };
});
exports.generateKeyPair = generateKeyPair;
// generates keypair using keys parsed from passphrase
const createKeyPair = (name) => __awaiter(void 0, void 0, void 0, function* () {
    const passPhraseData = nearSeedPhrase.generateSeedPhrase();
    const account = {
        account_id: name,
        public_key: passPhraseData.publicKey,
        private_key: passPhraseData.secretKey,
        passPhrase: passPhraseData.seedPhrase,
    };
    return account;
});
exports.createKeyPair = createKeyPair;
/**
 * @return {boolean}
 */
//creates account using master account details
const createAccount = (new_account) => __awaiter(void 0, void 0, void 0, function* () {
    const account = yield (0, exports.getMasterAccount)();
    // account.deleteAccount(new_account.hex_key);
    const res = yield account.createAccount(new_account.account_id, new_account.public_key, 
    //default amount with which the new account is to be created
    new_account.amount);
    try {
        if (res["status"].hasOwnProperty("SuccessValue")) {
            return true;
        }
    }
    catch (e) {
        console.log(e);
    }
    return false;
});
exports.createAccount = createAccount;
// function that generates keys from a given seed phrase
const getKeysFromSeedPhrase = (seedPhrase) => __awaiter(void 0, void 0, void 0, function* () {
    return nearSeedPhrase.parseSeedPhrase(seedPhrase);
});
exports.getKeysFromSeedPhrase = getKeysFromSeedPhrase;
//# sourceMappingURL=blockchain.js.map