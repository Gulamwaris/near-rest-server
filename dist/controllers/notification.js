"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
// Controlerr fro notifications - Bhavana
const axios_1 = __importDefault(require("axios"));
const get_template_text_1 = __importStar(require("../helpers/get-template-text"));
const mailer_1 = require("../helpers/mailer");
const logger_1 = __importDefault(require("../utils/logger"));
const userRepositories = __importStar(require("../repositories/users"));
/**
 * @description - Sends Notifictaion to customer
 * @param template - used to select an  template and sends the notification on mobile for the same
 * @param templateVariables - variables used in template
 * @param accountId - AccountId of user
 * .
 * @returns status
 */
const sendMobileNotification = ({ template, templateVariables, accountId, }) => __awaiter(void 0, void 0, void 0, function* () {
    var _a, _b;
    try {
        let token;
        // stores the status of each service which is called
        let status = {
            email: {
                success: false,
            },
        };
        // checks atleast one of the params is present if not then returns status with error
        // params are  emailID, emailID
        if (!accountId) {
            const errMessage = "No params were passed to send mobile notification. Token is required for sending";
            logger_1.default.error(`at: "controllers/notification/sendMobileNotification -> validation", ${errMessage}`);
            return Object.assign(Object.assign({}, status), { error: errMessage });
        }
        // calls the function to get the text message by providing the template variables required for the templates
        const templateResult = (0, get_template_text_1.default)({
            template,
            variables: templateVariables,
        });
        // checks if the template result is successful. if not then throws the error
        if (!templateResult.success) {
            throw templateResult.message;
        }
        if (accountId) {
            const userDetails = yield userRepositories.fetchUserByAccountId(accountId);
            token = (_a = userDetails === null || userDetails === void 0 ? void 0 : userDetails.data) === null || _a === void 0 ? void 0 : _a.uniqueToken;
        }
        var data = {
            to: token,
            notification: {
                body: templateResult.message.replace("\n", ""),
                title: templateResult.emailSubject,
            },
        };
        var headers = {
            Authorization: "key=AAAA0vDfS2M:APA91bHCS8eqHBYq08afd4vhfyJtW2j5INJRiSlqQi0f0Ta1s5_F_57trQN5c8P_ejNEoYKww1C3ZaCbGExADyPEt1iZAGro40F7Emsp5xPzT_GZ5HXu5KyP9F6mPk4tIiTo8giTXvuv",
            "Content-Type": "application/json",
        };
        const result = yield axios_1.default.post("https://fcm.googleapis.com/fcm/send", data, { headers });
        if (((_b = result.data) === null || _b === void 0 ? void 0 : _b.failure) === 1) {
            throw {
                statusCode: 500,
                customMessage: "Failed to send notification to mobile",
            };
        }
        return { isError: false };
    }
    catch (err) {
        logger_1.default.error(`at: "controllers/notification/sendNotification", ${JSON.stringify(err)} \n ${err}`);
        return { isError: true, error: err };
    }
});
/**
 * @description - Sends  email to the user when all the details are provided
 * *    - Add/Get the template from get-template-text.ts file in helpers
 * .
 * @param template - used to select an SMS template and sends the message for the same
 * @param templateVariables - variables used in SMS template
 * @param accountId - AccountId of user
 * .
 * @returns status - Sends the SMS and Email status
 */
const sendMailNotification = ({ template, templateVariables, accountId, }) => __awaiter(void 0, void 0, void 0, function* () {
    var _c;
    // stores the status of each service which is called
    let status = {
        email: {
            success: false,
        },
    };
    let emailID;
    // checks atleast one of the params is present if not then returns status with error
    // params are  emailID, emailID
    if (!accountId) {
        const errMessage = "No params were passed to send email. EmailID is required for sending";
        logger_1.default.error(`at: "controllers/notification/sendNotification -> validation", ${errMessage}`);
        return Object.assign(Object.assign({}, status), { error: errMessage });
    }
    try {
        // calls the function to get the text message by providing the template variables required for the templates
        const templateResult = (0, get_template_text_1.default)({
            template,
            variables: templateVariables,
        });
        // checks if the template result is successful. if not then throws the error
        if (!templateResult.success) {
            throw templateResult.message;
        }
        if (accountId) {
            const userDetails = yield userRepositories.fetchUserByAccountId(accountId);
            emailID = (_c = userDetails === null || userDetails === void 0 ? void 0 : userDetails.data) === null || _c === void 0 ? void 0 : _c.email;
        }
        // try-catch for mailing to the provided email address
        try {
            if (emailID) {
                // Get email content
                let emailFormattedWithContent = get_template_text_1.emailTemplate;
                emailFormattedWithContent = emailFormattedWithContent.replace("{{message}}", templateResult.message);
                // Making a call to the node mailer function to send an email to the given customer details
                const info = yield (0, mailer_1.mailer)(emailID, templateResult.emailSubject, emailFormattedWithContent);
                // logging the details of Email sent to email id(s)
                status.email.success = true;
            }
        }
        catch (err) {
            logger_1.default.error(`at: "controllers/notification/sendNotification -> send Mail", ${JSON.stringify(err)} \n ${err}`);
            status.email.error = err;
        }
    }
    catch (err) {
        logger_1.default.error(`at: "controllers/notification/sendNotification", ${JSON.stringify(err)} \n ${err}`);
    }
    return status;
});
// Export all controllers as an object
exports.default = {
    sendMailNotification,
    sendMobileNotification,
};
//# sourceMappingURL=notification.js.map