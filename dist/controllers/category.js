"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const categoryRepositories = __importStar(require("../repositories/category"));
const logger_1 = __importDefault(require("../utils/logger"));
// Controller for adding a new category - Bhavana
const addCategory = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // calling the addCategory repo with the req data
        const result = yield categoryRepositories.addCategory(data);
        // If there is any error then throw the error
        if (!result.success) {
            throw {
                statusCode: 400,
                customMessage: "Unable to add category",
            };
        }
        // If thre is no error then return error as false
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/category/addCategory" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for adding a new sub category under a category - Bhavana
const addSubCategory = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // calling the addSubCategory repo with the req data
        const result = yield categoryRepositories.addSubCategory(data);
        // If there is any error then throw the error
        if (!result.success) {
            throw {
                statusCode: 400,
                customMessage: "Unable to add Sub category",
            };
        }
        // If thre is no error then return error as false
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/category/addSubCategory" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for fetching all categories - Bhavana
const fetchAllCategory = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Call the repository to fetchAllCategory
        const result = yield categoryRepositories.fetchAllCategory();
        // Checks for the exact result value
        if (!result.success) {
            throw {
                statusCode: 400,
                customMessage: `Unable to fetch category at this moment.`,
            };
        }
        return {
            isError: false,
            data: result.data,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/category/fetchAllCategory" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for deleting all categories - Bhavana
const deleteAllCategory = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Call the repository to deleteAllCategory
        const deleteResult = yield categoryRepositories.deleteAllCategory();
        // Checks for the result value
        if (!deleteResult.success) {
            throw {
                statusCode: 400,
                customMessage: `Unable to delete all category.`,
            };
        }
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/category/deleteAllUser" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
// Controller for fetching category details by Id - Bhavana
const fetchCategoryById = (categoryId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Call the repository to fetch the category by id
        const result = yield categoryRepositories.fetchCategoryById(categoryId);
        // Checks for the exact result value
        if (!result.data) {
            throw {
                statusCode: 400,
                customMessage: `Category Not Found With id: ${categoryId}`,
            };
        }
        return {
            isError: false,
            data: result.data,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/category/fetchCategoryById" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
exports.default = {
    addCategory,
    fetchAllCategory,
    deleteAllCategory,
    addSubCategory,
    fetchCategoryById,
};
//# sourceMappingURL=category.js.map