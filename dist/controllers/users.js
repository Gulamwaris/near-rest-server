"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const userRepositories = __importStar(require("../repositories/users"));
const logger_1 = __importDefault(require("../utils/logger"));
const storeUserDetails = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const isUserExists = yield userRepositories.fetchUserByAccountId(data.accountId);
        if (isUserExists.data) {
            throw {
                statusCode: 400,
                customMessage: `User Already Exists with account ${data.accountId}`,
            };
        }
        // calling teh createCTA repo with the req data
        const result = yield userRepositories.storeUser(data);
        // If there is any error then throw the error
        if (!result.success) {
            throw {
                statusCode: 400,
                customMessage: "Unable to save user Details in DB",
            };
        }
        // If thre is no error then return error as false
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/users/storeUserDetails" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
const updateProfile = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const isUserExists = yield userRepositories.fetchUserByAccountId(data.accountId);
        if (!isUserExists.data) {
            throw {
                statusCode: 400,
                customMessage: `User with ${data.accountId} doesn't exists`,
            };
        }
        // calling teh createCTA repo with the req data
        const result = yield userRepositories.updateProfile(data);
        // If there is any error then throw the error
        if (!result.success) {
            throw {
                statusCode: 400,
                customMessage: "Unable to update user Details",
            };
        }
        // If thre is no error then return error as false
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/users/updateProfile" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
const updateAddress = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const isUserExists = yield userRepositories.fetchUserByAccountId(data.accountId);
        if (!isUserExists.data) {
            throw {
                statusCode: 400,
                customMessage: `User with ${data.accountId} doesn't exists`,
            };
        }
        // calling teh createCTA repo with the req data
        const result = yield userRepositories.updateAddress(data);
        // If there is any error then throw the error
        if (!result.success) {
            throw {
                statusCode: 400,
                customMessage: "Unable to update Adress Details",
            };
        }
        // If thre is no error then return error as false
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/users/updateProfile" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
const addNewAddress = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const isUserExists = yield userRepositories.fetchUserByAccountId(data.accountId);
        if (!isUserExists.data) {
            throw {
                statusCode: 400,
                customMessage: `User with ${data.accountId} doesn't exists`,
            };
        }
        // calling teh createCTA repo with the req data
        const result = yield userRepositories.addNewAddress(data);
        // If there is any error then throw the error
        if (!result.success) {
            throw {
                statusCode: 400,
                customMessage: "Unable to update Adress Details",
            };
        }
        // If thre is no error then return error as false
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/users/addNewAddress" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
const updateToken = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const isUserExists = yield userRepositories.fetchUserByAccountId(data.accountId);
        if (!isUserExists.data) {
            throw {
                statusCode: 400,
                customMessage: `User with ${data.accountId} doesn't exists`,
            };
        }
        // calling teh createCTA repo with the req data
        const result = yield userRepositories.updateUniqueToken(data);
        // If there is any error then throw the error
        if (!result.success) {
            throw {
                statusCode: 400,
                customMessage: "Unable to update Token",
            };
        }
        // If thre is no error then return error as false
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/users/updateToken" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
const fetchAllUsers = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Call the repository to fetch the user
        const result = yield userRepositories.fetchAllUsers();
        // Checks for the exact result value
        if (!result.success) {
            throw {
                statusCode: 400,
                customMessage: `Unable to fetch users at this moment.`,
            };
        }
        return {
            isError: false,
            data: result.data,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/users/fetchAllUsers" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
const fetchUserFavorites = (accountId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Call the repository to fetch the user
        const result = yield userRepositories.getFavoritesProducts(accountId);
        // Checks for the exact result value
        if (!result.success) {
            throw {
                statusCode: 400,
                customMessage: `Unable to fetch favorite products at this moment.`,
            };
        }
        return {
            isError: false,
            data: result.data,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/users/fetchUserFavorites" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
const fetchUserByAccountId = (accountId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Call the repository to fetch the user
        const result = yield userRepositories.fetchUserByAccountId(accountId);
        // Checks for the exact result value
        if (!result.data) {
            throw {
                statusCode: 400,
                customMessage: `User Not Found With id: ${accountId}`,
            };
        }
        return {
            isError: false,
            data: result.data,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/users/deleteUser" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
const deleteUser = (accountId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        //Call the repository to fetch the user
        const result = yield userRepositories.fetchUserByAccountId(accountId);
        // Checks for the exact result value
        if (!result.data) {
            throw {
                statusCode: 400,
                customMessage: `User Not Found With id: ${accountId}`,
            };
        }
        const deleteResult = yield userRepositories.deleteUser(accountId);
        // Checks for the result value
        if (!deleteResult.success) {
            throw {
                statusCode: 400,
                customMessage: `Unable to delete user with id ${accountId}.`,
            };
        }
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/users/deleteUser" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
const deleteAllUser = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const deleteResult = yield userRepositories.deleteAllUsers();
        // Checks for the result value
        if (!deleteResult.success) {
            throw {
                statusCode: 400,
                customMessage: `Unable to delete all users.`,
            };
        }
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/users/deleteAllUser" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
const manageFavorites = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const isAccExists = yield userRepositories.fetchUserByAccountId(data.accountId);
        if (!isAccExists.data) {
            throw {
                statusCode: 400,
                customMessage: `No User Accounts Exixts`,
            };
        }
        const result = yield userRepositories.manageFavorites(data);
        // If there is any error then throw the error
        if (!result.success) {
            throw {
                statusCode: 400,
                customMessage: `Unable to add Favorites`,
            };
        }
        // If there is no error then return error as false
        return {
            isError: false,
        };
    }
    catch (error) {
        logger_1.default.error(`at: "controllers/users/manageFavorites" => ${JSON.stringify(error)}\n${error}`);
        // return negative response
        return {
            isError: true,
            error: error,
        };
    }
});
exports.default = {
    storeUserDetails,
    updateProfile,
    updateAddress,
    updateToken,
    deleteUser,
    deleteAllUser,
    fetchAllUsers,
    fetchUserByAccountId,
    manageFavorites,
    addNewAddress,
    fetchUserFavorites,
};
//# sourceMappingURL=users.js.map