"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.fetchTransactionByAccountId = exports.addTransaction = void 0;
const transactions_1 = require("../db-init/models/transactions");
const logger_1 = __importDefault(require("../utils/logger"));
const addTransaction = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { transactionDesc, transactionType, orderId, amount, accountId } = data;
        // adding Category Details to DB
        yield transactions_1.Transactions.create({
            transactionDesc,
            transactionType,
            orderId,
            amount,
            accountId,
            transactionDate: Date.now(),
        });
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/transactions/addTransaction" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
        };
    }
});
exports.addTransaction = addTransaction;
const fetchTransactionByAccountId = (accountId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // fetching all users present in db
        let result = yield transactions_1.Transactions.find({ accountId: accountId });
        return { data: result, success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/transactions/fetchTransactionByAccountId" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return { success: false };
    }
});
exports.fetchTransactionByAccountId = fetchTransactionByAccountId;
//# sourceMappingURL=transactions.js.map