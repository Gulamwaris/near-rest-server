"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.updateRentalStatus = exports.getProductsByFilter = exports.getUserProductsStats = exports.getProductByAccountId = exports.updateProductBufferDate = exports.updateProductPublishStatus = exports.updateProductRatings = exports.getProductBySubCategory = exports.getProductByCategory = exports.getProductsBySortType = exports.deleteProduct = exports.getProductById = exports.getAllProducts = exports.updateProductInfo = exports.addProduct = void 0;
const logger_1 = __importDefault(require("../utils/logger"));
const products_1 = require("../db-init/models/products");
const users_1 = require("../db-init/models/users");
const addProduct = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { categoryId, subCategoryId, productName, price, liabilityPrice, images, sellerName, createdOn, modifiedOn, sellerAccountId, productAvailability, productDetails, isPublished, rentalDuration, bufferPeriod, isItemForRent, } = data;
        const addedProduct = yield products_1.Products.create({
            isItemForRent,
            categoryId,
            subCategoryId,
            productName,
            price,
            liabilityPrice,
            images,
            rating: {
                totalRatings: 0,
                averageRatings: 0,
            },
            sellerName,
            createdOn,
            modifiedOn,
            sellerAccountId,
            productAvailability,
            productDetails,
            isPublished,
            rentalDuration,
            bufferPeriod,
        });
        if (addedProduct) {
            yield users_1.Users.findOneAndUpdate({ accountId: sellerAccountId }, {
                $push: {
                    productsAdded: { productId: addedProduct._id },
                },
            }, { upsert: true });
        }
        return {
            success: true,
        };
    }
    catch (error) {
        logger_1.default.error(`at:"repositories/products/addProduct" => ${JSON.stringify(error)}\n${error}`);
        return {
            success: false,
            errorMessage: "Something went wrong please try again later",
        };
    }
});
exports.addProduct = addProduct;
const updateProductInfo = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { productId, productName, price, liabilityPrice, images, sellerName, productAvailability, productDetails, isPublished, rentalDuration, bufferPeriod, } = data;
        yield products_1.Products.findByIdAndUpdate(productId, {
            $set: {
                productName,
                price,
                liabilityPrice,
                images,
                sellerName,
                modifiedOn: new Date(),
                productAvailability,
                productDetails,
                isPublished,
                rentalDuration,
                bufferPeriod,
            },
        });
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/products/updateProductInfo" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
            errorMessage: "Something went wrong please try again later",
        };
    }
});
exports.updateProductInfo = updateProductInfo;
const getAllProducts = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const productData = yield products_1.Products.find({ isPublished: true }, {
            _id: 1,
            productName: 1,
            price: 1,
            liabilityPrice: 1,
            rating: 1,
            images: 1,
            createdOn: 1,
            modifiedOn: 1,
            onRent: 1,
        }).sort({ createdOn: -1 });
        // if product found with url the then return true
        if (productData) {
            return {
                data: productData,
                success: true,
            };
        }
        // else return false
        return {
            success: false,
            errorMessage: "Product not found",
        };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/products/getAllProducts" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
            errorMessage: JSON.stringify(error),
        };
    }
});
exports.getAllProducts = getAllProducts;
const getProductById = (productId, addView) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const productData = yield products_1.Products.findById(productId);
        if (addView) {
            yield products_1.Products.findByIdAndUpdate(productId, {
                $inc: { totalViews: 1 },
            });
        }
        // if product found with url the then return tru
        if (productData) {
            return {
                data: productData,
                success: true,
            };
        }
        // else return false
        return {
            success: false,
            errorMessage: "Product not found",
        };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/products/getProductById" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
            errorMessage: JSON.stringify(error),
        };
    }
});
exports.getProductById = getProductById;
const deleteProduct = (productId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // Deleting Section and updating sections Array for product into DB
        yield products_1.Products.findByIdAndDelete(productId);
        // returning true
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/products/deleteProduct" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
            errorMessage: "Something went wrong please try again later",
        };
    }
});
exports.deleteProduct = deleteProduct;
const getProductsBySortType = (sortType) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        let sortQuery;
        switch (sortType) {
            case "lowestprice":
                sortQuery = { price: 1 };
                break;
            case "highestprice":
                sortQuery = { price: -1 };
                break;
            case "highestrating":
                sortQuery = { "rating.averageRatings": -1 };
                break;
            case "oldest":
                sortQuery = { createdOn: 1 };
                break;
            case "popular":
                sortQuery = { totalViews: -1 };
                break;
            case "recentlyadded":
            default:
                sortQuery = { createdOn: -1 };
                break;
        }
        const productData = yield products_1.Products.find({ isPublished: true }, {
            _id: 1,
            productName: 1,
            price: 1,
            liabilityPrice: 1,
            rating: 1,
            images: 1,
            createdOn: 1,
            modifiedOn: 1,
            onRent: 1,
        }).sort(sortQuery);
        // if product found with url the then return true
        if (productData) {
            return {
                data: productData,
                success: true,
            };
        }
        // else return false
        return {
            success: false,
            errorMessage: "Product not found",
        };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/products/getAllProducts" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
            errorMessage: JSON.stringify(error),
        };
    }
});
exports.getProductsBySortType = getProductsBySortType;
const getProductByCategory = (categoryId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const productData = yield products_1.Products.find({ categoryId: categoryId, isPublished: true }, {
            _id: 1,
            productName: 1,
            price: 1,
            liabilityPrice: 1,
            rating: 1,
            images: 1,
            createdOn: 1,
            modifiedOn: 1,
            onRent: 1,
        });
        // if product found with url the then return tru
        return {
            data: productData,
            success: true,
        };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/products/getProductByCategory" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
            errorMessage: JSON.stringify(error),
        };
    }
});
exports.getProductByCategory = getProductByCategory;
const getProductBySubCategory = (subCategoryId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const productData = yield products_1.Products.find({ subCategoryId: subCategoryId, isPublished: true }, {
            _id: 1,
            productName: 1,
            price: 1,
            liabilityPrice: 1,
            rating: 1,
            images: 1,
            createdOn: 1,
            modifiedOn: 1,
            onRent: 1,
        });
        return {
            data: productData,
            success: true,
        };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/products/getProductBySubCategory" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
            errorMessage: JSON.stringify(error),
        };
    }
});
exports.getProductBySubCategory = getProductBySubCategory;
const updateProductRatings = (productId, rating) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield products_1.Products.findByIdAndUpdate(productId, {
            $set: {
                rating: rating,
            },
        });
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/products/updateProductRatings" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
            errorMessage: "Something went wrong please try again later",
        };
    }
});
exports.updateProductRatings = updateProductRatings;
const updateProductPublishStatus = (productId, isPublished) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield products_1.Products.findByIdAndUpdate(productId, {
            $set: {
                isPublished: isPublished,
            },
        });
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/products/updateProductPublishStatus" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
            errorMessage: "Something went wrong please try again later",
        };
    }
});
exports.updateProductPublishStatus = updateProductPublishStatus;
const updateProductBufferDate = (productId, bufferDate) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield products_1.Products.findByIdAndUpdate(productId, {
            $set: {
                bufferDuration: bufferDate,
            },
        });
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/products/updateProductBufferDate" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
            errorMessage: "Something went wrong please try again later",
        };
    }
});
exports.updateProductBufferDate = updateProductBufferDate;
const getProductByAccountId = (accountId, isPublished) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const productData = yield products_1.Products.find({ sellerAccountId: accountId, isPublished: isPublished }, {
            _id: 1,
            productName: 1,
            price: 1,
            liabilityPrice: 1,
            rating: 1,
            images: 1,
            createdOn: 1,
            totalRentals: 1,
            totalViews: 1,
            favoritesAdded: 1,
            onRent: 1,
            isItemForRent: 1,
        }).sort({ totalViews: -1 });
        const data = yield products_1.Products.aggregate([
            { $match: { sellerAccountId: accountId } },
            {
                $group: {
                    _id: "$sellerAccountId",
                    totalViews: { $sum: "$totalViews" },
                    totalFavorites: { $sum: "$favoritesAdded" },
                    totalRentals: { $sum: "$totalRentals" },
                },
            },
            { $sort: { total: -1 } },
        ]);
        return {
            data: productData,
            success: true,
        };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/products/getProductByAccountId" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
            errorMessage: JSON.stringify(error),
        };
    }
});
exports.getProductByAccountId = getProductByAccountId;
const getUserProductsStats = (accountId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const data = yield products_1.Products.aggregate([
            { $match: { sellerAccountId: accountId } },
            {
                $group: {
                    _id: "$sellerAccountId",
                    totalViews: { $sum: "$totalViews" },
                    totalFavorites: { $sum: "$favoritesAdded" },
                    totalRentals: { $sum: "$totalRentals" },
                },
            },
            { $sort: { total: -1 } },
        ]);
        return {
            data: data[0],
            success: true,
        };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/products/getProductByAccountId" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
            errorMessage: JSON.stringify(error),
        };
    }
});
exports.getUserProductsStats = getUserProductsStats;
const getProductsByFilter = (minPrice, maxPrice) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const productData = yield products_1.Products.find({ isPublished: true, price: { $gte: minPrice, $lte: maxPrice } }, {
            _id: 1,
            productName: 1,
            price: 1,
            liabilityPrice: 1,
            rating: 1,
            images: 1,
            createdOn: 1,
            modifiedOn: 1,
            onRent: 1,
        }).sort({ createdOn: -1 });
        // if product found with url the then return true
        if (productData) {
            return {
                data: productData,
                success: true,
            };
        }
        // else return false
        return {
            success: false,
            errorMessage: "Product not found",
        };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/products/getAllProducts" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
            errorMessage: JSON.stringify(error),
        };
    }
});
exports.getProductsByFilter = getProductsByFilter;
const updateRentalStatus = (productId, status) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield products_1.Products.findByIdAndUpdate(productId, {
            $set: {
                onRent: status,
            },
        });
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/products/updateRentalStatus" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
            errorMessage: "Something went wrong please try again later",
        };
    }
});
exports.updateRentalStatus = updateRentalStatus;
//# sourceMappingURL=product.js.map