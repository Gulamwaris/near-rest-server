"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.fetchReviewByProductId = exports.addProductReview = void 0;
const orders_1 = require("../db-init/models/orders");
const reviews_1 = require("../db-init/models/reviews");
const logger_1 = __importDefault(require("../utils/logger"));
const addProductReview = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { productId, rating, reviewDesc, reviewTitle, postedBy, orderId } = data;
        // adding Users Details to DB
        yield reviews_1.Reviews.findOneAndUpdate({ productId: productId }, {
            $push: {
                reviews: {
                    rating,
                    reviewDesc,
                    reviewTitle,
                    postedBy,
                    postedOn: new Date(),
                },
            },
        }, { upsert: true });
        yield orders_1.Orders.findByIdAndUpdate(orderId, {
            ratingProvided: true,
        });
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/reviews/addProductReview" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
        };
    }
});
exports.addProductReview = addProductReview;
const fetchReviewByProductId = (productId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logger_1.default.debug(`Fetch Reviews for productId ${productId}`);
        let result = yield reviews_1.Reviews.findOne({ productId: productId });
        return { success: true, data: result };
        // Add the DB query here
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/products/fetchReviewByProductId" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return { success: false };
    }
});
exports.fetchReviewByProductId = fetchReviewByProductId;
//# sourceMappingURL=reviews.js.map