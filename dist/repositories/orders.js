"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getAllDeliveredOrders = exports.updateDamageApprovedByRentee = exports.updateOrderReturnDetails = exports.updateDeliveryDetails = exports.deleteSeedPhraseForOrder = exports.updateSeedPhraseForOrder = exports.getPendingOrders = exports.getOrdersByStatus = exports.getRenterOrdersByAccountId = exports.getRenteeOrdersByAccountId = exports.getOrderByOrderId = exports.updateOrderStatus = exports.saveOrder = void 0;
const orders_1 = require("../db-init/models/orders");
const products_1 = require("../db-init/models/products");
const users_1 = require("../db-init/models/users");
const logger_1 = __importDefault(require("../utils/logger"));
const saveOrder = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { orderDuration, orderStartDate, orderEndDate, productId, renteeAccountId, renterAccountId, address, sellerName, productPrice, productName, productImage, liabilityPrice, shippingPrice, totalPrice, orderType, } = data;
        // adding Users Details to DB
        const result = yield orders_1.Orders.create({
            orderDuration,
            orderStatus: "Pending",
            orderStartDate,
            orderEndDate,
            productId,
            renteeAccountId,
            renterAccountId,
            address,
            orderedOn: new Date(),
            sellerName,
            productPrice,
            productName,
            productImage,
            liabilityPrice,
            shippingPrice,
            totalPrice,
            deliveryDetails: {},
            deliveredOn: null,
            approvedOn: null,
            shippedOn: null,
            returnedOn: null,
            ratingProvided: false,
            deliveryConfirmed: false,
            orderType,
        });
        yield users_1.Users.findOneAndUpdate({
            accountId: renterAccountId,
            "productsAdded.productId": productId,
        }, {
            $push: {
                "productsAdded.$.orders": result._id,
            },
        });
        yield products_1.Products.findByIdAndUpdate(productId, {
            $inc: { totalRentals: 1 },
        });
        return { success: true, data: result };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/orders/addProductReview" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
        };
    }
});
exports.saveOrder = saveOrder;
const updateOrderStatus = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { orderId, orderStatus } = data;
        let query = {};
        // adding Users Details to DB
        if (orderStatus === "Delivered") {
            query = {
                orderStatus,
                deliveredOn: new Date(),
                modifiedOn: new Date(),
            };
        }
        else if (orderStatus === "Approved" || orderStatus === "Rejected") {
            query = {
                orderStatus,
                approvedOn: new Date(),
                modifiedOn: new Date(),
            };
        }
        else if (orderStatus === "Shipped") {
            query = {
                orderStatus,
                shippedOn: new Date(),
                modifiedOn: new Date(),
            };
        }
        else {
            query = {
                orderStatus,
                modifiedOn: new Date(),
            };
        }
        yield orders_1.Orders.findByIdAndUpdate(orderId, query);
        const result = yield orders_1.Orders.findById(orderId);
        if (orderStatus == "Approved" && result.orderType != "Sell") {
            yield products_1.Products.findByIdAndUpdate(result.productId, {
                $set: { onRent: true },
            });
        }
        else if (orderStatus == "Cancelled") {
            yield products_1.Products.findByIdAndUpdate(result.productId, {
                $set: { onRent: false },
            });
        }
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/orders/updateOrderStatus" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
        };
    }
});
exports.updateOrderStatus = updateOrderStatus;
const getOrderByOrderId = (orderId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // adding Users Details to DB
        const result = yield orders_1.Orders.findById(orderId);
        return { success: true, data: result };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/orders/getOrderByOrderId" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
        };
    }
});
exports.getOrderByOrderId = getOrderByOrderId;
const getRenteeOrdersByAccountId = (accountId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // adding Users Details to DB
        const result = yield orders_1.Orders.find({ renteeAccountId: accountId }).sort({
            orderedOn: -1,
        });
        return { success: true, data: result };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/orders/getRenteeOrdersByAccountId" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
        };
    }
});
exports.getRenteeOrdersByAccountId = getRenteeOrdersByAccountId;
const getRenterOrdersByAccountId = (accountId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // adding Users Details to DB
        const result = yield orders_1.Orders.find({ renterAccountId: accountId });
        return { success: true, data: result };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/orders/getRenterOrdersByAccountId" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
        };
    }
});
exports.getRenterOrdersByAccountId = getRenterOrdersByAccountId;
const getOrdersByStatus = (status, accountId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        var ordersData = yield orders_1.Orders.find({
            renterAccountId: accountId,
            orderStatus: status,
        });
        // if product found with url the then return true
        if (ordersData) {
            return {
                data: ordersData,
                success: true,
            };
        }
        // else return false
        return {
            success: false,
            errorMessage: "Product not found",
        };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/products/getAllProducts" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
            errorMessage: JSON.stringify(error),
        };
    }
});
exports.getOrdersByStatus = getOrdersByStatus;
const getPendingOrders = (accountId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        var ordersData = yield orders_1.Orders.find({
            renterAccountId: accountId,
            orderStatus: "Pending",
        });
        // if product found with url the then return true
        if (ordersData) {
            return {
                data: ordersData,
                success: true,
            };
        }
        // else return false
        return {
            success: false,
            errorMessage: "Product not found",
        };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/orders/getPendingOrders" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
            errorMessage: JSON.stringify(error),
        };
    }
});
exports.getPendingOrders = getPendingOrders;
const updateSeedPhraseForOrder = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { orderId, seedPhrase, holdingAccountId } = data;
        // adding Users Details to DB
        yield orders_1.Orders.findByIdAndUpdate(orderId, {
            $set: {
                seedPhrase,
                holdingAccountId,
            },
        });
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/orders/updateSeedPhraseForOrder" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
        };
    }
});
exports.updateSeedPhraseForOrder = updateSeedPhraseForOrder;
const deleteSeedPhraseForOrder = (orderId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // adding Users Details to DB
        yield orders_1.Orders.findByIdAndUpdate(orderId, {
            $unset: {
                seedPhrase: "",
            },
        });
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/orders/deleteSeedPhraseForOrder" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
        };
    }
});
exports.deleteSeedPhraseForOrder = deleteSeedPhraseForOrder;
const updateDeliveryDetails = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { orderId, deliveryDetails } = data;
        // adding Users Details to DB
        yield orders_1.Orders.findByIdAndUpdate(orderId, {
            deliveryDetails,
            deliveredOn: new Date(),
            deliveryConfirmed: true,
            orderStatus: "Ongoing",
            modifiedOn: new Date(),
        });
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/orders/updateDeliveryDetails" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
        };
    }
});
exports.updateDeliveryDetails = updateDeliveryDetails;
const updateOrderReturnDetails = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { orderId, returnDetails } = data;
        // adding Users Details to DB
        yield orders_1.Orders.findByIdAndUpdate(orderId, {
            returnDetails,
            modifiedOn: new Date(),
            returnedOn: new Date(),
        });
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/orders/updateOrderReturnDetails" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
        };
    }
});
exports.updateOrderReturnDetails = updateOrderReturnDetails;
const updateDamageApprovedByRentee = (isApproved, orderId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // adding Users Details to DB
        yield orders_1.Orders.findByIdAndUpdate(orderId, {
            damageApprovedByRentee: isApproved,
            modifiedOn: new Date(),
        });
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/orders/updateDamageApprovedByRentee" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
        };
    }
});
exports.updateDamageApprovedByRentee = updateDamageApprovedByRentee;
const getAllDeliveredOrders = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // adding Users Details to DB
        const result = yield orders_1.Orders.find({
            orderStatus: "Delivered",
            deliveryConfirmed: false,
        });
        return { success: true, data: result };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/orders/getAllDeliveredOrders" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
        };
    }
});
exports.getAllDeliveredOrders = getAllDeliveredOrders;
//# sourceMappingURL=orders.js.map