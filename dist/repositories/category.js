"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteAllCategory = exports.fetchCategoryById = exports.fetchAllCategory = exports.addSubCategory = exports.addCategory = void 0;
const category_1 = require("../db-init/models/category");
const logger_1 = __importDefault(require("../utils/logger"));
const addCategory = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { categoryName, categoryIcon } = data;
        // adding Category Details to DB
        yield category_1.Category.create({
            categoryName,
            categoryIcon,
            createdOn: Date.now(),
        });
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/category/addCategory" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
        };
    }
});
exports.addCategory = addCategory;
const addSubCategory = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { categoryId, name, fields, icon } = data;
        // adding Category Details to DB
        yield category_1.Category.findByIdAndUpdate(categoryId, {
            $push: {
                subCategory: {
                    name,
                    fields,
                    icon,
                    createdOn: new Date(),
                },
            },
        }, { upsert: true });
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/category/addSubCategory" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
        };
    }
});
exports.addSubCategory = addSubCategory;
const fetchAllCategory = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logger_1.default.debug(`Fetching all Category`);
        // fetching all users present in db
        let result = yield category_1.Category.find({}, { subCategory: 0, __v: 0 });
        return { data: result, success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/category/fetchAllCategory" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return { success: false };
    }
});
exports.fetchAllCategory = fetchAllCategory;
const fetchCategoryById = (categoryId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        // fetching all users present in db
        let result = yield category_1.Category.findById(categoryId, { __v: 0 });
        return { data: result, success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/category/fetchCategoryById" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return { success: false };
    }
});
exports.fetchCategoryById = fetchCategoryById;
const deleteAllCategory = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield category_1.Category.deleteMany();
        // return true
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/category/deleteAllCategory" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return { success: false };
    }
});
exports.deleteAllCategory = deleteAllCategory;
//# sourceMappingURL=category.js.map