"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getFavoritesProducts = exports.manageFavorites = exports.deleteAllUsers = exports.deleteUser = exports.updateUniqueToken = exports.updateAddress = exports.addNewAddress = exports.updateProfile = exports.fetchUserByAccountId = exports.fetchAllUsers = exports.storeUser = void 0;
const products_1 = require("../db-init/models/products");
const users_1 = require("../db-init/models/users");
const logger_1 = __importDefault(require("../utils/logger"));
const storeUser = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { accountId, firstName, lastName, email, mobileNumber, profileImage, uniqueToken, isRenter, } = data;
        // adding Users Details to DB
        yield users_1.Users.create({
            accountId,
            productsAdded: [],
            myOrders: [],
            firstName,
            lastName,
            email,
            mobileNumber,
            profileImage,
            address: [],
            favoriteProducts: [],
            uniqueToken,
            isRenter,
            createdOn: Date.now(),
        });
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/users/storeUser" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
        };
    }
});
exports.storeUser = storeUser;
const fetchAllUsers = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logger_1.default.debug(`Fetching all Users`);
        // fetching all users present in db
        let result = yield users_1.Users.find({});
        return { data: result, success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/users/fetchAllUsers" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return { success: false };
    }
});
exports.fetchAllUsers = fetchAllUsers;
const fetchUserByAccountId = (accountId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        logger_1.default.debug(`Fetch Users with User ID ${accountId}`);
        let result = yield users_1.Users.findOne({ accountId: accountId });
        return { success: true, data: result };
        // Add the DB query here
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/users/fetchUserByAccountId" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return { success: false };
    }
});
exports.fetchUserByAccountId = fetchUserByAccountId;
const updateProfile = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { accountId, firstName, lastName, email, mobileNumber, profileImage, } = data;
        yield users_1.Users.findOneAndUpdate({ accountId: accountId }, {
            firstName,
            lastName,
            mobileNumber,
            profileImage,
            email,
            modifiedOn: Date.now(),
        });
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/users/updateProfile" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return {
            success: false,
        };
    }
});
exports.updateProfile = updateProfile;
const addNewAddress = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { accountId, address } = data;
        yield users_1.Users.findOneAndUpdate({ accountId: accountId }, {
            $push: {
                address: address,
            },
        });
        // return true
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/users/addNewAddress" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return { success: false };
    }
});
exports.addNewAddress = addNewAddress;
const updateAddress = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { accountId, address } = data;
        yield users_1.Users.findOneAndUpdate({ accountId: accountId }, {
            address: address,
            modifiedOn: Date.now(),
        });
        // return true
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/users/updateAddress" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return { success: false };
    }
});
exports.updateAddress = updateAddress;
const updateUniqueToken = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { accountId, token } = data;
        yield users_1.Users.findOneAndUpdate({ accountId: accountId }, {
            uniqueToken: token,
            modifiedOn: Date.now(),
        });
        // return true
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/users/updateUniqueToken" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return { success: false };
    }
});
exports.updateUniqueToken = updateUniqueToken;
const deleteUser = (accountId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield users_1.Users.findOneAndDelete({ accountId: accountId });
        // return true
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/users/deleteUser" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return { success: false };
    }
});
exports.deleteUser = deleteUser;
const deleteAllUsers = () => __awaiter(void 0, void 0, void 0, function* () {
    try {
        yield users_1.Users.deleteMany();
        // return true
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/users/deleteAllUsers" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return { success: false };
    }
});
exports.deleteAllUsers = deleteAllUsers;
const manageFavorites = (data) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { accountId, productId } = data;
        const userInfo = yield (0, exports.fetchUserByAccountId)(accountId);
        let favoriteProducts = userInfo.data.favoriteProducts;
        if (favoriteProducts.includes(productId)) {
            let index = favoriteProducts.indexOf(productId);
            favoriteProducts.splice(index, 1);
        }
        else {
            favoriteProducts.push(productId);
            yield products_1.Products.findByIdAndUpdate(productId, {
                $inc: { favoritesAdded: 1 },
            });
        }
        yield users_1.Users.findOneAndUpdate({ accountId: accountId }, {
            $set: {
                favoriteProducts: favoriteProducts,
            },
        });
        // return true
        return { success: true };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/users/updateAddress" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return { success: false };
    }
});
exports.manageFavorites = manageFavorites;
const getFavoritesProducts = (accountId) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const userInfo = yield (0, exports.fetchUserByAccountId)(accountId);
        let favoriteProducts = userInfo.data.favoriteProducts;
        let result = [];
        for (let i = 0; i < favoriteProducts.length; i++) {
            const productData = yield products_1.Products.findById(favoriteProducts[i], {
                _id: 1,
                productName: 1,
                price: 1,
                liabilityPrice: 1,
                rating: 1,
                images: 1,
                createdOn: 1,
                modifiedOn: 1,
            });
            result.push(productData);
        }
        // return true
        return { success: true, data: result };
    }
    catch (error) {
        // logging the error
        logger_1.default.error(`at:"repositories/users/updateAddress" => ${JSON.stringify(error)}\n${error}`);
        // returning false
        return { success: false };
    }
});
exports.getFavoritesProducts = getFavoritesProducts;
//# sourceMappingURL=users.js.map