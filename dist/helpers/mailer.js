"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.mailer = void 0;
const client_1 = require("@sendgrid/client");
const mail_1 = __importDefault(require("@sendgrid/mail"));
const config_1 = __importDefault(require("config"));
const logger_1 = __importDefault(require("../utils/logger"));
// Mailer class
const mailer = (receiver, subject, text) => {
    const sendGridApiKey = config_1.default.get("SENDGRID_API_KEY");
    const mail = config_1.default.get("SMTP_SENDER_EMAIL");
    const apiKey = sendGridApiKey;
    mail_1.default.setClient(new client_1.Client());
    mail_1.default.setApiKey(apiKey);
    const msg = {
        to: receiver,
        from: mail,
        subject: subject,
        text,
    };
    mail_1.default
        .send(msg)
        .then(() => {
        return true;
    })
        .catch((error) => {
        logger_1.default.error(error);
        return false;
    });
};
exports.mailer = mailer;
//# sourceMappingURL=mailer.js.map