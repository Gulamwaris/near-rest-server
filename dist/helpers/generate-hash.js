"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.generateHash = void 0;
// Bhavana Gupta - 04/05/2021 Function for creating a hash
const crypto_1 = __importDefault(require("crypto"));
//Get the secret key from the env variable and store it in a constant
// const HashKey: string = config.get("HASHING_KEY");
//Creating a function to generate hash and export the function
//The function accepts the payload as a param (request payload object) and returns a HMAC SHA256 string
const generateHash = (data, hash) => {
    //Use crypto to create an instance of hmac using algorithm sha256 and the secret key from EBPS
    const hmac = crypto_1.default.createHmac(hash ? hash : "sha256", "HashKey");
    //update hmac using the payload after stringify-ing it
    hmac.update(data);
    //Type cast the hash to a string and return
    return hmac.digest("hex");
};
exports.generateHash = generateHash;
//# sourceMappingURL=generate-hash.js.map