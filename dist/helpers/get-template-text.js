"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.emailTemplate = void 0;
/**
 * Used to send SMS text based on the template name provided with it's required variables
 * @param template - template name to switch between and send the message for the same
 * @param variables - variables required to form the text message which is to be sent to customer
 */
function getTextFromTemplate({ template, variables, }) {
    // common error message which will pe passed
    const errMsg = `All variables are required for type ${template}. Required params are`;
    // storing all the variables in a constant
    const { renteeName, orderId, sellerName, price, productName } = variables;
    // switching between template values and returning respective result
    switch (template) {
        case "approve-order":
            // if none of the variables are present the return success false
            if (!renteeName || !orderId || !sellerName || !price) {
                return {
                    success: false,
                    message: `${errMsg} renteeName,orderId,sellerName and price.`,
                    emailSubject: "",
                };
            }
            // returns success true with message
            return {
                success: true,
                message: `Hi ${renteeName}, \n\nYour Order #${orderId} with ${sellerName} of ${price} NEAR is approved. \nThanks for using Aqsus.`,
                emailSubject: `Your order with ${sellerName} is approved`,
            };
        case "reject-order":
            // if none of the variables are present the return success false
            if (!renteeName || !orderId || !sellerName || !price) {
                return {
                    success: false,
                    message: `${errMsg} renteeName,orderId,sellerName and price.`,
                    emailSubject: "",
                };
            }
            // returns success true with message
            return {
                success: true,
                message: `Hi ${renteeName}, \n\nYour Order #${orderId} with ${sellerName} of ${price} NEAR is rejected.\nThanks for using Aqsus.`,
                emailSubject: `Your order with ${sellerName} is rejected`,
            };
        case "order-placed":
            // if none of the variables are present the return success false
            if (!productName || !orderId || !sellerName) {
                return {
                    success: false,
                    message: `${errMsg} productName,orderId,sellerName.`,
                    emailSubject: "",
                };
            }
            // returns success true with message
            return {
                success: true,
                message: `Hi ${sellerName}, \n\nYou have received an Order with orderID ${orderId} for product ${productName} .\nThanks for using Aqsus.`,
                emailSubject: `Order Received for product ${productName}`,
            };
        case "payment":
            // if none of the variables are present the return success false
            if (!productName || !orderId || !sellerName || !price) {
                return {
                    success: false,
                    message: `${errMsg} productName,orderId,sellerName.`,
                    emailSubject: "",
                };
            }
            // returns success true with message
            return {
                success: true,
                message: `Hi ${sellerName}, \n\nYou have received payment for order #${orderId} of ${price} NEAR for product ${productName} .\nThanks for using Aqsus.`,
                emailSubject: `Payment received for order #${orderId}`,
            };
        case "order-delivered":
            // if none of the variables are present the return success false
            if (!productName || !orderId || !renteeName) {
                return {
                    success: false,
                    message: `${errMsg} productName,orderId,sellerName.`,
                    emailSubject: "",
                };
            }
            // returns success true with message
            return {
                success: true,
                message: `Hi ${renteeName}, \n\nYour order #${orderId} for product ${productName} has been delivered successfully.\nPlease confirm your delivery through the app.\nThanks for using Aqsus.`,
                emailSubject: `Delivery Successfull for product ${productName}`,
            };
        default:
            return {
                emailSubject: "",
                message: "Template not supported.",
                success: false,
            };
    }
}
exports.default = getTextFromTemplate;
// Template for emails
exports.emailTemplate = `{{message}}

Warm regards,

Aqsus.

This is system generated email. Please do not reply to this email.

`;
//# sourceMappingURL=get-template-text.js.map