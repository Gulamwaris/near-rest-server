"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ok = exports.notify = exports.reject = exports.CONFIG_PATH = void 0;
exports.CONFIG_PATH = "./near-api-server.config.json";
const reject = (err) => ({
    error: typeof err === "string" ? err : JSON.stringify(err),
});
exports.reject = reject;
const notify = (message) => ({ text: message });
exports.notify = notify;
const ok = (req, res, payload) => {
    if (req.headers["x-encrypt-data"]) {
        const encryptedData = req.app.locals.encryptMessage(JSON.stringify(payload), req.body.publicKey);
        return res.status(200).json({ encryptedData });
    }
    res.status(200).json(payload);
};
exports.ok = ok;
//# sourceMappingURL=api.js.map