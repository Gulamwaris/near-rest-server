"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Transactions = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
var transactions = new mongoose_1.default.Schema({
    orderId: String,
    transactionDate: Date,
    amount: Number,
    transactionType: { type: String, enum: ["CREDIT", "DEBIT", "DEPOSIT"] },
    transactionDesc: String,
    accountId: String,
}, { collection: "transactions" });
//applying schema for specified database connection
exports.Transactions = mongoose_1.default.model("transactions", transactions);
//# sourceMappingURL=transactions.js.map