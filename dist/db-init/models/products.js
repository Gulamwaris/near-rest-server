"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Products = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
var products = new mongoose_1.default.Schema({
    isItemForRent: Boolean,
    categoryId: String,
    subCategoryId: String,
    productName: String,
    price: Number,
    liabilityPrice: Number,
    images: [String],
    sellerName: String,
    createdOn: Date,
    modifiedOn: Date,
    sellerAccountId: String,
    productAvailability: [String],
    rentalDuration: Number,
    bufferDuration: Date,
    bufferPeriod: Number,
    rating: {
        averageRatings: Number,
        totalRatings: Number,
    },
    totalRentals: { type: Number, default: 0 },
    totalViews: { type: Number, default: 0 },
    favoritesAdded: { type: Number, default: 0 },
    productDetails: {
        description: String,
        damageDescription: String,
        additionalInfo: Object,
    },
    isPublished: Boolean,
    onRent: { type: Boolean, default: false },
}, { collection: "products" });
//applying schema for specified database connection
exports.Products = mongoose_1.default.model("products", products);
//# sourceMappingURL=products.js.map