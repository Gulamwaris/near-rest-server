"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Orders = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
var orders = new mongoose_1.default.Schema({
    orderStatus: String,
    orderDuration: Number,
    orderStartDate: String,
    orderEndDate: String,
    returnedOn: Date,
    productId: String,
    orderedOn: Date,
    renterAccountId: String,
    renteeAccountId: String,
    address: Object,
    modifiedOn: Date,
    sellerName: String,
    productPrice: Number,
    productName: String,
    productImage: String,
    liabilityPrice: Number,
    shippingPrice: Number,
    seedPhrase: String,
    holdingAccountId: String,
    totalPrice: Number,
    deliveryConfirmed: Boolean,
    deliveredOn: Date,
    approvedOn: Date,
    shippedOn: Date,
    ratingProvided: Boolean,
    deliveryDetails: {
        damageDescription: String,
        images: [String],
    },
    returnDetails: {
        damageDescription: String,
        images: [String],
    },
    damageApprovedByRentee: Boolean,
    orderType: String,
}, { collection: "orders" });
//applying schema for specified database connection
exports.Orders = mongoose_1.default.model("orders", orders);
//# sourceMappingURL=orders.js.map