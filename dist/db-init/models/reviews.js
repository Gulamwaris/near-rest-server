"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Reviews = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
var reviews = new mongoose_1.default.Schema({
    productId: String,
    reviews: [
        {
            reviewTitle: String,
            reviewDesc: String,
            postedBy: String,
            postedOn: Date,
            rating: Number,
        },
    ],
}, { collection: "reviews" });
//applying schema for specified database connection
exports.Reviews = mongoose_1.default.model("reviews", reviews);
//# sourceMappingURL=reviews.js.map