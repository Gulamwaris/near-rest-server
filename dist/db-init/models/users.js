"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Users = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
var users = new mongoose_1.default.Schema({
    accountId: String,
    productsAdded: [
        {
            productId: String,
            orders: [String],
        },
    ],
    myOrders: [String],
    firstName: String,
    lastName: String,
    email: String,
    mobileNumber: String,
    profileImage: String,
    address: [
        {
            addressType: String,
            address: String,
            postCode: String,
            city: String,
        },
    ],
    favoriteProducts: [String],
    uniqueToken: String,
    createdOn: Date,
    isRenter: Boolean,
    modifiedOn: Date,
}, { collection: "users" });
//applying schema for specified database connection
exports.Users = mongoose_1.default.model("users", users);
//# sourceMappingURL=users.js.map