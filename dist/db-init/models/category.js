"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Category = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
var category = new mongoose_1.default.Schema({
    categoryName: String,
    categoryIcon: String,
    subCategory: [
        {
            name: String,
            icon: String,
            createdOn: Date,
            fields: [
                {
                    parameterName: String,
                    parameterType: String,
                    maxlength: Number,
                    optional: Boolean,
                },
            ],
        },
    ],
    createdOn: Date,
}, { collection: "category" });
//applying schema for specified database connection
exports.Category = mongoose_1.default.model("category", category);
//# sourceMappingURL=category.js.map