"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.disconnect = exports.createConnection = exports.database = void 0;
const mongoose_1 = __importDefault(require("mongoose"));
const config_1 = __importDefault(require("config"));
const logger_1 = __importDefault(require("../utils/logger"));
const mongoURL = config_1.default.get("MONGO_URL");
const createConnection = () => {
    // add your own uri below
    const uri = mongoURL;
    // const uri = "mongodb://localhost:27017/aqsus";
    // const uri = "mongodb://admin:QWRNaW5AIzEyMw@34.69.207.26:27017/admin";
    if (exports.database) {
        return;
    }
    mongoose_1.default.connect(uri);
    exports.database = mongoose_1.default.connection;
    exports.database.once("open", () => __awaiter(void 0, void 0, void 0, function* () {
        logger_1.default.info("Connected to database");
    }));
    exports.database.on("error", () => {
        logger_1.default.error("ALERT => Error connecting to database");
    });
};
exports.createConnection = createConnection;
const disconnect = () => {
    if (!exports.database) {
        return;
    }
    mongoose_1.default.disconnect();
};
exports.disconnect = disconnect;
//# sourceMappingURL=dbConn.js.map