"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const helmet_1 = __importDefault(require("helmet"));
const morgan_1 = __importDefault(require("morgan"));
const logger_1 = __importStar(require("./utils/logger"));
const body_parser_1 = require("body-parser");
const cors_1 = __importDefault(require("cors"));
const blockchain_1 = __importDefault(require("./routes/blockchain"));
const notification_1 = __importDefault(require("./routes/notification"));
const cryptography_1 = __importDefault(require("./utils/cryptography"));
const decrypt_1 = __importDefault(require("./middlewares/decrypt"));
const health_check_1 = __importDefault(require("./routes/health-check"));
const error_1 = __importDefault(require("./middlewares/error"));
const users_1 = __importDefault(require("./routes/users"));
const category_1 = __importDefault(require("./routes/category"));
const reviews_1 = __importDefault(require("./routes/reviews"));
const orders_1 = __importDefault(require("./routes/orders"));
const transactions_1 = __importDefault(require("./routes/transactions"));
const dbConn_1 = require("./db-init/dbConn");
const scripts_1 = __importDefault(require("./routes/scripts"));
const product_1 = __importDefault(require("./routes/product"));
const app = (0, express_1.default)();
app.set("trust-proxy", 1);
// Block all unwanted headers using helmet
app.use((0, helmet_1.default)());
// Disable x-powered-by header separately
app.disable("x-powered-by");
//Setup server
app.use((0, cors_1.default)());
app.use((0, body_parser_1.json)());
app.use((0, body_parser_1.urlencoded)({
    extended: false,
}));
app.disable("etag"); //Disables caching
app.use((0, morgan_1.default)("common", {
    stream: { write: (message) => logger_1.httpLogger.http(message) },
    skip: (req, res) => { var _a; return (_a = req.baseUrl) === null || _a === void 0 ? void 0 : _a.includes("health-check"); },
}));
app.use("/", decrypt_1.default, blockchain_1.default);
app.use("/notification", notification_1.default);
app.use("/users", users_1.default);
app.use("/category", category_1.default);
app.use("/products", product_1.default);
app.use("/reviews", reviews_1.default);
app.use("/orders", orders_1.default);
app.use("/transactions", transactions_1.default);
app.use("/scripts", scripts_1.default);
app.use("/health-check", health_check_1.default);
app.use(error_1.default);
//Check if port exists in the environment else use 5000
const port = process.env.PORT || 3000;
//If the environment is test, do not start the express server
if (process.env.NODE_ENV !== "test") {
    (0, dbConn_1.createConnection)();
    app
        .listen(parseInt(port.toString()), "0.0.0.0", () => {
        // Initialize encryption/decryption module
        const cryptoGraphy = new cryptography_1.default();
        // Storing getPublicKey and decrypt function in the app locals
        app.locals.getPublicKey = cryptoGraphy.getPublicKey;
        app.locals.decryptMessage = cryptoGraphy.decryptMessage;
        app.locals.encryptMessage = cryptoGraphy.encryptMessage;
        //Listen the express server on the given port and log a message to the logs
        logger_1.default.info(`Server is listening on port ${port}`);
    })
        .on("error", (err) => {
        //In case of an error, log the error to the logs
        logger_1.default.error(JSON.stringify(err));
    });
}
//# sourceMappingURL=index.js.map