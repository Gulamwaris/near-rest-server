//NEAR REST Server
// PIPELINE START
pipeline {
  agent {label 'master'}

  // ENV VARS START
  environment {
      // Common for all
      APP_NAME = "near-rest"
      GIT_CREDENTIALS = "gitlab_cred"
      GIT_BRANCH = "develop"
      GIT_REPO_URL = "https://gitlab.com/alsaabltd_and_headstrait/nr-rest-server.git"
      PROJECT_ID = 'amal-health'
      CLUSTER_NAME = 'ah-cluster'
      LOCATION = 'us-central1-c'
      CREDENTIALS_ID = 'amal-health'

      // Variables for DEV Environment
      DEV_EXECUTE_STAGE = "1" // 1 = execute stage, 0 = skip execution
      DEV_IMAGE_REPO = "gcr.io/amal-health"
      DEV_STAGE = "dev"
      DEV_BUILD_NAME = "${APP_NAME}-${DEV_STAGE}"
      DEV_IMAGE_NAME = "${DEV_IMAGE_REPO}/${APP_NAME}-${DEV_STAGE}"
      DEV_DOCKER_SECRET = "regcreds"
      DEV_INTERNAL_HOST = ""
      DEV_EXTERNAL_HOST = "amal-health"
      //DEV_API_URL = "http://34.66.148.111/ah-app-dev/api"
  } 
  // ENV VARS END

  // STAGES START
  stages{

    // SCM CHECKOUT START
    stage('SCM Checkout'){
      steps{
        git branch: GIT_BRANCH, credentialsId: GIT_CREDENTIALS, url: GIT_REPO_URL
      }
    }
    // SCM CHECKOUT END

    // DEV STAGE START
    stage('DEV Environment') {
      // Stage to be executed only when flag set to 1
      when {
        expression { DEV_EXECUTE_STAGE != "0" }
      }
      stages{

        stage('Configuring DEV Files') {
          steps{
            // Creating DEV RSA Files
            sh label: 'Generating private key', script: 'openssl genrsa -out rsa_2048_priv.pem 2048'
            sh label: 'Generating public key', script: 'openssl rsa -pubout -in rsa_2048_priv.pem -out rsa_2048_pub.pem'

            // changes in Dockerfile
            echo "No placeholders in Dockerfile"

            // changes in mainConfig.yml
            echo "Replacing placeholders in mainConfig.yml"
            sh label: 'Replaced base url placeholder', script: 'sed -i -e \"s,BASE_URL_PLACEHOLDER,${DEV_IMAGE_REPO},g\" deploy/kubernetes/mainConfig.yml'
            sh label: 'Replaced build name placeholder', script: 'sed -i -e \"s,BUILD_NAME_PLACEHOLDER,${DEV_BUILD_NAME},g\" deploy/kubernetes/mainConfig.yml'
            sh label: 'Replaced tag placeholder', script: 'sed -i -e \"s,TAG_PLACEHOLDER,${BUILD_NUMBER},g\" deploy/kubernetes/mainConfig.yml'
            sh label: 'Replaced stage placeholder', script: 'sed -i -e \"s,STAGE_PLACEHOLDER,${DEV_STAGE},g\" deploy/kubernetes/mainConfig.yml'
            sh label: 'Replaced docker secret placeholder', script: 'sed -i -e \"s,DOCKER_SECRET_PLACEHOLDER,${DEV_DOCKER_SECRET},g\" deploy/kubernetes/mainConfig.yml'
            
            // changes in ingress.yml
            echo "Replacing placeholders in ingress.yml"
            sh label: 'Replaced build name placeholder', script: 'sed -i -e \"s,BUILD_NAME_PLACEHOLDER,${DEV_BUILD_NAME},g\" deploy/kubernetes/ingress.yml'
            sh label: 'Replaced stage placeholder', script: 'sed -i -e \"s,STAGE_PLACEHOLDER,${DEV_STAGE},g\" deploy/kubernetes/ingress.yml'
            sh label: 'Replaced external host placeholder', script: 'sed -i -e \"s,EXTERNAL_HOST_PLACEHOLDER,${DEV_EXTERNAL_HOST},g\" deploy/kubernetes/ingress.yml'
            sh label: 'Replaced internal host placeholder', script: 'sed -i -e \"s,INTERNAL_HOST_PLACEHOLDER,${DEV_INTERNAL_HOST},g\" deploy/kubernetes/ingress.yml'

            // changes in cronJob.yml
            //echo "Replacing placeholders in cronJob.yml"
            //sh label: 'Replaced build name placeholder', script: 'sed -i -e \"s,BUILD_NAME_PLACEHOLDER,${DEV_BUILD_NAME},g\" deploy/kubernetes/cronJob.yml'
            //sh label: 'Replaced stage placeholder', script: 'sed -i -e \"s,STAGE_PLACEHOLDER,${DEV_STAGE},g\" deploy/kubernetes/cronJob.yml'
            //sh label: 'Replaced DEV_API_URL', script: 'sed -i -e \"s,DEV_API_URL,${DEV_API_URL},g\" deploy/kubernetes/cronJob.yml'
          }
        }
        stage('Building and Pushing DEV Docker Image'){
          steps{
            echo "Running build-${BUILD_NUMBER} on ${env.JENKINS_URL}"
            script {
              sh label: 'Docker login', script: 'cat ah.json | docker login -u _json_key --password-stdin https://gcr.io'
              sh label: 'Building docker image', script: 'docker build -t ${DEV_IMAGE_NAME}:latest .'
              sh label: 'Adding new docker tag', script: 'docker tag ${DEV_IMAGE_NAME}:latest $DEV_IMAGE_NAME:${BUILD_NUMBER}'
              sh label: 'Pushing image with tag latest', script: 'docker push ${DEV_IMAGE_NAME}:latest'
              sh label: 'Pushing image with tag equal to current build number', script: 'docker push ${DEV_IMAGE_NAME}:${BUILD_NUMBER}'
            }
          }
        }
        stage('Deploy Staging') {
            steps{
                step([$class: 'KubernetesEngineBuilder', 
                projectId: env.PROJECT_ID, 
                clusterName: env.CLUSTER_NAME, 
                location: env.LOCATION, 
                manifestPattern: 'deploy/kubernetes/ingress.yml', 
                credentialsId: env.CREDENTIALS_ID, 
                verifyDeployments: false])
                
                step([$class: 'KubernetesEngineBuilder', 
                projectId: env.PROJECT_ID, 
                clusterName: env.CLUSTER_NAME, 
                location: env.LOCATION, 
                manifestPattern: 'deploy/kubernetes/mainConfig.yml', 
                credentialsId: env.CREDENTIALS_ID, 
                verifyDeployments: false])

                //step([$class: 'KubernetesEngineBuilder', 
                //projectId: env.PROJECT_ID, 
                //clusterName: env.CLUSTER_NAME, 
                //location: env.LOCATION, 
                //manifestPattern: 'deploy/kubernetes/cronJob.yml', 
                //credentialsId: env.CREDENTIALS_ID, 
                //verifyDeployments: false])
            }
        }
        stage('DEV Stage Cleanup'){
          steps{
            sh label: 'Removing dangling docker images', script: 'docker rmi -f ${DEV_IMAGE_NAME}:latest $DEV_IMAGE_NAME:${BUILD_NUMBER}'
            // Reverting file changes for next stage
            sh label: 'Dropping changes', script: 'git reset --hard'
          }
        } 
      }
    }
    // DEV STAGE END
  }
}
// PIPELINE END