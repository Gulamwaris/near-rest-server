import { Response, Request } from "express";
export const CONFIG_PATH = "./near-api-server.config.json";

export const reject = (err: any) => ({
  error: typeof err === "string" ? err : JSON.stringify(err),
});

export const notify = (message: string) => ({ text: message });

export const ok = (req: Request, res: Response, payload: any) => {
  if (req.headers["x-encrypt-data"]) {
    const encryptedData = req.app.locals.encryptMessage(
      JSON.stringify(payload),
      req.body.publicKey
    );
    return res.status(200).json({ encryptedData });
  }
  res.status(200).json(payload);
};
