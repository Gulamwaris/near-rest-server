import NodeRSA from "node-rsa";
import fs from "fs";

export default class CryptoGraphy {
  private privateKeyCrypto: NodeRSA;
  private publicKeyCrypto: NodeRSA;
  private privateKey: string;
  private publicKey: string;
  constructor() {
    this.publicKey = fs.readFileSync(`${process.cwd()}/rsa_2048_pub.pem`, {
      encoding: "utf8",
    });
    this.privateKey = fs.readFileSync(`${process.cwd()}/rsa_2048_priv.pem`, {
      encoding: "utf8",
    });
    this.privateKeyCrypto = new NodeRSA(this.privateKey);
    this.publicKeyCrypto = new NodeRSA(this.publicKey);
    this.privateKeyCrypto.setOptions({ encryptionScheme: "pkcs1" });
    this.publicKeyCrypto.setOptions({ encryptionScheme: "pkcs1" });
  }

  // Method to decrypt a given string
  decryptMessage = (encryptedMessage: string) => {
    return this.privateKeyCrypto.decrypt(encryptedMessage, "utf8");
  };
  // Method to encrypt a given string and with the given public key
  encryptMessage = (message: string, publicKey: string) => {
    const customPublicKeyCrypto = new NodeRSA(publicKey);
    customPublicKeyCrypto.setOptions({ encryptionScheme: "pkcs1" });
    return customPublicKeyCrypto.encrypt(message, "base64");
  };

  getPublicKey = () => {
    return this.publicKey;
  };
}
